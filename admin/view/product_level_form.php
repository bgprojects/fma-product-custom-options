<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
require_once FMEPCO_PLUGIN_DIR . 'admin/class-fme-product-custom-options-admin.php';
$custom_options = new FME_Product_Custom_Options_Admin();
$product_options = $custom_options->getProductOptions($post->ID);
$meta_value = get_post_meta( $post->ID, '_exclude_global_options', true );
$data_wc_price_table = $custom_options->woocommerce_get_custom_item_price_formats_arr($post->ID, true);
//print_r($product_options);

//echo $post->ID;

function find_data_in_preview($id_of_prev){
	global $post;
			$post_get_meta = get_post_meta($post->ID, 'previews_for_chili', true);
			if($post_get_meta != ""){
				$objdec = json_decode($post_get_meta);
				if($objdec != null){
					$objdec = (array)$objdec;
					if(count($objdec) > 0){
						for($ii=0; $ii < count($objdec); $ii++){
							$objdec_id = $objdec[$ii]->id;
							if($objdec_id == $id_of_prev){
								return $objdec[$ii];
							}
						}
					}
				}
			}
	return "";
}

?>
			
<div class="field_wrapper">

	<div class="field_success"></div>
	<div class="addButton">
		<input onClick="addFields()" type="button" id="btnAdd" class="button button-primary button-large" value="<?php echo _e('Add New Option','fmepco'); ?>"> 
	</div>
	<form id="featured_upload" method="post" action="" enctype="multipart/form-data">
	
		
		<?php 
			if(count($product_options)!= 0) {
			foreach ($product_options as $product_option) { 

			$product_option_rows = $custom_options->getProductOptionRows($product_option->id);
		?>
		
		<input type="hidden" value="yes" name="editpro" />
		<input type="hidden" value="<?php echo $product_option->id; ?>" name="product_option[<?php echo $product_option->id; ?>][option_id]" />
		<div class="addFormFields" id="field<?php echo $product_option->id; ?>">
		<input onClick="updateFormatFieldsGo('<?php echo $product_option->id; ?>')" type="button" class="updateFormatFiledGuuu button button-primary" value="<?php echo _e('Update format icons','fmepco'); ?>">		
                <input onClick="delFields('<?php echo $product_option->id; ?>')" type="button" class="btnDel button btn-danger" value="<?php echo _e('Delete Option','fmepco'); ?>">
				<?php
					$arr_fmrs = get_option("wp_wc_fma_product_cust_options_formats");
				?>
				<div class="all_format_fields_here<?php echo $product_option->id; ?>">
				<?php if($arr_fmrs != "" && count($arr_fmrs) > 0){ ?>
                                    <?php for ($izz = 0; $izz < count($arr_fmrs); $izz++) { ?>
                                    <div data-nr="<?php echo $arr_fmrs[$izz]["key"]; ?>" class="one_format_in_all_formats one_format_in_all_formats_go<?php echo $arr_fmrs[$izz]["key"]; ?>"></div>
                                    <?php }; ?>
                                    <table class="table addFormats" style="width: 100%;text-align: center;">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Icon (category page)</th>
                                                <th>Shadow (category page)</th>
                                                <th>Icon (active)</th>
                                                <th>Icon (inactive)</th>
                                                <th>Remove</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for ($izz = 0; $izz < count($arr_fmrs); $izz++) { ?>
                                            

<tr>
    <td>
        <input type="text" value="<?php echo $arr_fmrs[$izz]["label"]; ?>" class="label_of_format" name="label_of_one_format[]" placeholder="Format name">
    </td>
    <td>
        <?php if ($arr_fmrs[$izz]["file"] != "") { ?>
        <label for="updateIconInput<?php echo $izz; ?>" class="inputclass"><img src="../wp-content/uploads<?php echo $arr_fmrs[$izz]["file"]; ?>" class="adminFormatIcon" title="Click to update image"><img src="../wp-content/uploads<?php echo $arr_fmrs[$izz]["file"]; ?>" class="enlarge"></label>
        <?php } else { ?>
            <input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" class="file_of_format inputfile" name="pic_of_the_format[]" id="updateIconInput<?php echo $izz; ?>" placeholder="Icon"><label for="updateIconInput<?php echo $izz; ?>" class="button">Upload icon</label>
        <?php } ?>
        <input type="hidden" value="<?php echo $arr_fmrs[$izz]["file"]; ?>" class="file_url_all_here" name="file_url_all_here[]">
        <input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" id="updateIconInput<?php echo $izz; ?>" class="file_of_format inputfile" name="pic_of_the_format[]" placeholder="Icon">
    </td>
    <td>
        <?php if ($arr_fmrs[$izz]["shadow_file"] != "") { ?>
        <label for="updateShadowInput<?php echo $izz; ?>" class="inputclass"><img src="../wp-content/uploads<?php echo $arr_fmrs[$izz]["shadow_file"]; ?>" class="adminFormatIcon" title="Click to update image"><img src="../wp-content/uploads<?php echo $arr_fmrs[$izz]["shadow_file"]; ?>" class="enlarge"></label>
        <?php } else { ?>
            <input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" class="shadow_file_of_format inputfile" name="shadow_pic_of_the_format[]" id="updateShadowInput<?php echo $izz; ?>" placeholder="Shadow Icon"><label for="updateShadowInput<?php echo $izz; ?>" class="button">Upload shadow</label>
        <?php } ?>
        <input type="hidden" value="<?php echo $arr_fmrs[$izz]["shadow_file"]; ?>" class="shadow_file_url_all_here" name="shadow_file_url_all_here[]">
        <input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" class="shadow_file_of_format inputfile" id="updateShadowInput<?php echo $izz; ?>" name="shadow_pic_of_the_format[]" placeholder="Icon">
    </td>
    <td>
        <?php if ($arr_fmrs[$izz]["file2"] != "") { ?>
        <label for="updateIconActive<?php echo $izz; ?>" class="inputclass"><img src="../wp-content/uploads<?php echo $arr_fmrs[$izz]["file2"]; ?>" class="adminFormatIcon" title="Click to update image"><img src="../wp-content/uploads<?php echo $arr_fmrs[$izz]["file2"]; ?>" class="enlarge"></label>
        <?php } else { ?>
            <input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" class="file2_of_format inputfile" name="pic_of_the_format[]" id="updateIconActive<?php echo $izz; ?>" placeholder="Icon"><label for="updateIconActive<?php echo $izz; ?>" class="button">Upload icon</label>
        <?php } ?>
        <input type="hidden" value="<?php echo $arr_fmrs[$izz]["file2"]; ?>" class="file2_url_all_here" name="file_url_all_here[]">
        <input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" class="file2_of_format inputfile" name="pic_of_the_format[]" id="updateIconActive<?php echo $izz; ?>" placeholder="Icon">
    </td>
    <td>
        <?php if ($arr_fmrs[$izz]["shadow2_file"] != "") { ?>
        <label for="updateIconInactive<?php echo $izz; ?>" class="inputclass"><img src="../wp-content/uploads<?php echo $arr_fmrs[$izz]["shadow2_file"]; ?>" class="adminFormatIcon" title="Click to update image"><img src="../wp-content/uploads<?php echo $arr_fmrs[$izz]["shadow2_file"]; ?>" class="enlarge" title="Click to update image"></label>
        <?php } else { ?> 
            <input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" class="shadow2_file_of_format inputfile" name="shadow2_pic_of_the_format[]" id="updateIconInactive<?php echo $izz; ?>" placeholder="Shadow Icon"><label for="updateIconInactive<?php echo $izz; ?>" class="button">Upload icon</label>
        <?php } ?>
        <input type="hidden" value="<?php echo $arr_fmrs[$izz]["shadow2_file"]; ?>" class="shadow2_file_url_all_here" name="shadow2_file_url_all_here[]">
        <input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" class="shadow2_file_of_format inputfile" name="shadow2_pic_of_the_format[]" id="updateIconInactive<?php echo $izz; ?>" placeholder="Icon">

    </td>
    <td>
        <input onClick="removeFormatsOneFieldThat('<?php echo $product_option->id; ?>',<?php echo $arr_fmrs[$izz]["key"]; ?>)" type="button" class="removethatOnefield button button-default" value="<?php echo _e('Remove format field', 'fmepco'); ?>">
    </td>

</tr>

                                                            <?php
                                                        }
                                                        ?>
                                                        </tbody>
                                                </table>
                                        <div class="uploading_file_here"></div>
					
				<?php }else{ ?>
                                        <div data-nr="1" class="one_format_in_all_formats one_format_in_all_formats_go1">
                                                        <table class="table addFormats" style="width: 100%;text-align: center;">
                                                            <thead>
                                                                <tr>
                                                                    <th>Name</th>
                                                                    <th>Icon (category page)</th>
                                                                    <th>Shadow (category page)</th>
                                                                    <th>Icon (active)</th>
                                                                    <th>Icon (inactive)</th>
                                                                    <th>Remove</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr><td>';
                                                                        <input type="text" class="label_of_format" name="label_of_one_format[]" placeholder="Format name">
                                                                    </td><td>
                                                                        <input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" id="uploadIconInput" class="file_of_format inputfile" name="pic_of_the_format[]" placeholder="Icon"><label for="uploadIconInput" class="button">Upload icon</label>
                                                                        <input type="hidden" class="file_url_all_here" name="file_url_all_here[]">
                                                                    </td><td>
                                                                        <input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" id="uploadShadowInput" class="shadow_file_of_format inputfile" name="shadow_pic_of_the_format[]" placeholder="Shadow Icon"><label for="uploadShadowInput" class="button">Upload shadow</label>
                                                                        <input type="hidden" class="shadow_file_url_all_here" name="shadow_file_url_all_here[]">
                                                                    </td><td>
                                                                        <input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" id="uploadIcon2Input" class="shadow_file_of_format inputfile" name="shadow_pic_of_the_format[]" placeholder="Shadow Icon"><label for="uploadIcon2Input" class="button">Upload icon</label>
                                                                        <input type="hidden" class="file2_url_all_here" name="file2_url_all_here[]">
                                                                    </td><td>
                                                                        <input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" id="uploadShadow2Input" class="shadow_file_of_format inputfile" name="shadow_pic_of_the_format[]" placeholder="Shadow Icon"><label for="uploadShadow2Input" class="button">Upload shadow</label>
                                                                        <input type="hidden" class="shadow2_file_url_all_here" name="shadow2_file_url_all_here[]">
                                                                    </td><td>
                                                                        <input onClick="removeFormatsOneFieldThat('+field_id+','+nr+')" type="button" class="removethatOnefield button button-default" value="<?php echo _e('Remove format field', 'fmepco'); ?>">
                                                                        <div class="uploading_file_here"></div>
                                                                    </td></tr></div>
                                                            </tbody>
                                                        </table>
                                                        <div class="uploading_file_here"></div>

                                                    <?php } ?>
				</div>
				<input onClick="addFormatFieldsGo('<?php echo $product_option->id; ?>')" type="button" class="addFormatFiledGuuu button button-default" value="<?php echo _e('Add format field','fmepco'); ?>">
				
				
				<div class="topFields">
					<table class="datatable">
						<thead>
						    <tr>
						    	<th class="datath1"><label><b><?php echo _e('Title:','fmepco'); ?></b></label></th>
						    	<th class="datath2"><label><b><?php echo _e('Input Type:','fmepco'); ?></b></label></th>
						    	<th class="datath3"><label><b><?php echo _e('Is Required:','fmepco'); ?></b></label></th>
						    	<th class="datath4"><label><b>Sort Order:</b></label></th>
						    	</tr>
						</thead>
						<tbody>
							<tr>
								<td class="datath1"><input class="inputs" type="text" value="<?php echo stripslashes($product_option->option_title); ?>" name="product_option[<?php echo $product_option->id; ?>][option_title]"  id="title" /></td>
								<td class="datath2">
						    		<select class="select_type inputs" name="product_option[<?php echo $product_option->id; ?>][option_type]" id="type" onChange="showFields('<?php echo $product_option->id; ?>',this.value)">
						    			<option value=""><?php echo _e('-- Please select --','fmepco'); ?></option>
						    			<optgroup label="Text">
						    				<option value="field" <?php selected('field',$product_option->option_field_type); ?>><?php echo _e('Field','fmepco'); ?></option>
						    				<option value="area" <?php selected('area',$product_option->option_field_type); ?>><?php echo _e('Area','fmepco'); ?></option>
						    			</optgroup>
						    			
						    			<optgroup label="Select">
						    				<option value="drop_down" <?php selected('drop_down',$product_option->option_field_type); ?>><?php echo _e('Drop-down','fmepco'); ?></option>
			            					<option value="multiple" <?php selected('multiple',$product_option->option_field_type); ?>><?php echo _e('Multiple Select','fmepco'); ?></option>
			            				</optgroup>
					            		
			            			</select>
						        </td>
								<td class="datath3">
									<select class="select_is_required inputs" name="product_option[<?php echo $product_option->id; ?>][option_is_required]" id="is_required">
				                		<option value="yes" <?php selected('yes',$product_option->option_is_required); ?>><?php echo _e('Yes','fmepco'); ?></option>
				                		<option value="no" <?php selected('no',$product_option->option_is_required); ?>><?php echo _e('No','fmepco'); ?></option>
				                	</select>
								</td>
								<td class="datath4"><input class="inputs" type="text" value="<?php echo $product_option->option_sort_order; ?>" name="product_option[<?php echo $product_option->id; ?>][option_sort_order]" id="sort_order<?php echo $product_option->id; ?>" onChange="SortOrderonlyNumber(this.id);" /></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="bottom_fields">
					<div id="textField<?php echo $product_option->id; ?>" <?php if($product_option->option_field_type == 'field') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
						<table class="datatable">
							<thead>
							    <tr>
							    	<th class="datath1"><label><b><?php echo _e('Price:','fmepco'); ?></b></label></th>
							    	<th class="datath2"><label><b><?php echo _e('Price Type:','fmepco'); ?></b></label></th>
							    	<th class="datath3"><label><b><?php echo _e('Max Characters:','fmepco'); ?></b></label></th>
							    </tr>
							</thead>
							<tbody>
								<tr>
									<td class="datath1"><input class="inputs" type="text" value="<?php echo $product_option->option_price; ?>" name="product_option[<?php echo $product_option->id; ?>][text_option_price]" id="price<?php echo $product_option->id ?>" onChange="PriceOnly(this.id);" /></td>
									<td class="datath2">
										<select class="select_is_required inputs" name="product_option[<?php echo $product_option->id; ?>][text_option_price_type]" id="fieldprice_type">
					                		<option value="fixed" <?php selected('fixed',$product_option->option_price_type); ?>><?php echo _e('Fixed','fmepco'); ?></option>
					                		<option value="percent" <?php selected('percent',$product_option->option_price_type); ?>><?php echo _e('Percent','fmepco'); ?></option>
					                	</select>
									</td>
									<td class="datath3"><input class="inputs" type="text" value="<?php echo $product_option->option_maxchars; ?>" name="product_option[<?php echo $product_option->id; ?>][text_option_maxchars]" id="maxchars<?php echo $product_option->id; ?>" onChange="MaxCharsonlyNumber(this.id);" /></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div id="textArea<?php echo $product_option->id; ?>" <?php if($product_option->option_field_type == 'area') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
						<table class="datatable">
							<thead>
							    <tr>
							    	<th class="datath1"><label><b><?php echo _e('Price:','fmepco'); ?></b></label></th>
							    	<th class="datath2"><label><b><?php echo _e('Price Type:','fmepco'); ?></b></label></th>
							    	<th class="datath3"><label><b><?php echo _e('Max Characters:','fmepco'); ?></b></label></th>
							    </tr>
							</thead>
							<tbody>
								<tr>
									<td class="datath1"><input class="inputs" type="text" value="<?php echo $product_option->option_price; ?>" name="product_option[<?php echo $product_option->id; ?>][area_option_price]" id="price<?php echo $product_option->id ?>" onChange="PriceOnly(this.id);" /></td>
									<td class="datath2">
										<select class="select_is_required inputs" name="product_option[<?php echo $product_option->id; ?>][area_option_price_type]" id="areaprice_type">
					                		<option value="fixed" <?php selected('fixed',$product_option->option_price_type); ?>><?php echo _e('Fixed','fmepco'); ?></option>
					                		<option value="percent" <?php selected('percent',$product_option->option_price_type); ?>><?php echo _e('Percent','fmepco'); ?></option>
					                	</select>
									</td>
									<td class="datath3"><input class="inputs" type="text" value="<?php echo $product_option->option_maxchars; ?>" name="product_option[<?php echo $product_option->id; ?>][area_option_maxchars]" id="maxchars<?php echo $product_option->id; ?>" onChange="MaxCharsonlyNumber(this.id);" /></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div id="dropdown<?php echo $product_option->id; ?>" <?php if($product_option->option_field_type == 'drop_down') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
						<div class="rowfield_success<?php echo $product_option->id; ?>"></div>
						<table class="datatable" id="POITable<?php echo $product_option->id; ?>">
							<thead>
							    <tr>
							    	<th class="datath1"><label><b><?php echo _e('ID:','fmepco'); ?></b></label></th>
							    	<th class="datath2"><label><b><?php echo _e('Title:','fmepco'); ?></b></label></th>
							    	<th class="datath2"><label><b><?php echo _e('Rotate:','fmepco'); ?></b></label></th>
							    	<th class="datath2"><label><b><?php echo _e('Folding:','fmepco'); ?></b></label></th>
							    	<th class="datath2"><label><b><?php echo _e('Price:','fmepco'); ?></b></label></th>
							    	<th class="datath3"><label><b><?php echo _e('Price Type:','fmepco'); ?></b></label></th>
							    	<th class="datath4"><label><b><?php echo _e('Format icon:','fmepco'); ?></b></label></th>
									<?php
									if($data_wc_price_table != ""){
										?>
										<th class="datath3"><label><b><?php echo _e('Price table:','fmepco'); ?></b></label></th>
										<?php
									}
									?>
							    	<th></th>
							    </tr>
							</thead>
							<tbody>

							<?php if($product_option->option_field_type == 'drop_down') { ?>
							<?php foreach ($product_option_rows as $product_option_row) { ?>
								<?php
									$name_of_the_system = str_replace("alternative_chili_id", "", stripslashes($product_option_row->option_row_title));
									$format_arr = explode(";", $product_option_row->option_row_sort_order);
									$format_icc = $format_arr[0];
									$id_of_imgs = "";
									$rotate_inmgg = "";
									$wp_wc_price_inmgg = "";
                                                                        $folding = "vertical";
									if(count($format_arr) > 1){  $id_of_imgs = $format_arr[1]; }
									if(count($format_arr) > 2){  $rotate_inmgg = $format_arr[2]; }
									if(count($format_arr) > 3){  $folding = $format_arr[3]; }
									if(count($format_arr) > 4){  $wp_wc_price_inmgg = $format_arr[4]; }
									$data = find_data_in_preview($id_of_imgs);
									if($data != ""){
										if(isset($data->width) && isset($data->height)){
											$widthh = $data->width;
											$heightt = $data->height;
											//$name_of_the_system = $widthh." X ".$heightt;
										}
									}
								?>
								<tr id="tr<?php echo $product_option_row->id; ?>_<?php echo $product_option_row->option_id; ?>">
									<td class="datath1"><?php echo $id_of_imgs; ?></td>
									<td class="datath2">
									<input name="product_option[<?php echo $product_option_row->option_id; ?>][row_value][<?php echo $product_option_row->id; ?>][drop_option_row_title_faked_one]" class="inputs" type="text" value="<?php echo $name_of_the_system; ?>">
									<input class="inputs" type="hidden" value="<?php echo stripslashes($product_option_row->option_row_title); ?>" name="product_option[<?php echo $product_option_row->option_id; ?>][row_value][<?php echo $product_option_row->id; ?>][drop_option_row_title]" id="dropdowntitle" />
									</td>
									<td class="datath2">
										<input class="inputs" type="text" value="<?php echo $rotate_inmgg; ?>" name="product_option[<?php echo $product_option_row->option_id; ?>][row_value][<?php echo $product_option_row->id; ?>][drop_option_row_rotate]" id="dropdowntitle_rotate" />
									</td>
                                                                        <td class="datath2">
                                                                                <select name="product_option[<?php echo $product_option_row->option_id; ?>][row_value][<?php echo $product_option_row->id; ?>][drop_option_row_folding]">
                                                                                    <option value="vertical" ><?php echo _e('Choose folding','fmepco'); ?></option>
                                                                                    <option value="vertical" <?php if($folding == "vertical") echo "selected" ?>><?php echo _e('Vertical','fmepco'); ?></option>
                                                                                    <option value="horizontal" <?php if($folding == "horizontal") echo "selected" ?>><?php echo _e('Horizontal','fmepco'); ?></option>
                                                                                    <option value="vertical2page" <?php if($folding == "vertical2page") echo "selected" ?>><?php echo _e('Vertical 2 page','fmepco'); ?></option>
                                                                                    <option value="horizontal2page" <?php if($folding == "horizontal2page") echo "selected" ?>><?php echo _e('Horizontal 2 page','fmepco'); ?></option>
                                                                                </select>
									</td>
									<td class="datath2">
							    		<input class="inputs" type="text" value="<?php echo $product_option_row->option_row_price; ?>" name="product_option[<?php echo $product_option_row->option_id; ?>][row_value][<?php echo $product_option_row->id; ?>][drop_option_row_price]" id="price<?php echo $product_option_row->option_id; ?>__<?php echo $product_option_row->id; ?>" onChange="PriceOnly(this.id);" />
							        </td>
									<td class="datath3">
										<select class="select_is_required inputs" name="product_option[<?php echo $product_option_row->option_id; ?>][row_value][<?php echo $product_option_row->id; ?>][drop_option_row_price_type]" id="dropdownprice_type">
					                		<option value="fixed" <?php selected('fixed',$product_option_row->option_row_price_type); ?>><?php echo _e('Fixed','fmepco'); ?></option>
					                		<option value="percent" <?php selected('percent',$product_option_row->option_row_price_type); ?>><?php echo _e('Percent','fmepco'); ?></option>
					                	</select>
									</td>
									<td class="datath4">
									
									<?php /*<input class="inputs" type="text" value="<?php echo $product_option_row->option_row_sort_order; ?>" name="product_option[<?php echo $product_option_row->option_id; ?>][row_value][<?php echo $product_option_row->id; ?>][drop_option_row_sort_order]" id="sort_order<?php echo $product_option_row->option_id; ?>__<?php echo $product_option_row->id; ?>"  />*/
									?>
										<select name="product_option[<?php echo $product_option_row->option_id; ?>][row_value][<?php echo $product_option_row->id; ?>][drop_option_row_sort_order]" id="sort_order<?php echo $product_option_row->option_id; ?>__<?php echo $product_option_row->id; ?>" >
										<?php
											if($arr_fmrs != "" && count($arr_fmrs) > 0){
												for($izu=0; $izu < count($arr_fmrs); $izu++){
												?>
											<option value="<?php echo $arr_fmrs[$izu]["key"]; ?>" <?php if($arr_fmrs[$izu]["key"] == $format_icc){ echo "selected"; } ?>><?php echo $arr_fmrs[$izu]["label"]; ?></option>
												<?php
												}
											}else{
										?>
											<option value="a5_2s_1" <?php if($format_icc == "a5_2s_1"){ echo "selected"; } ?>><?php _e("A5 størrelse 2s 14,8X21 cm"); ?></option>
											<option value="a5_4s_2" <?php if($format_icc == "a5_4s_2"){ echo "selected"; } ?>><?php _e("A5 størrelse 4s 29,7X21 cm"); ?></option>
											<option value="a6_2s_8" <?php if($format_icc == "a6_2s_8"){ echo "selected"; } ?>><?php _e("A6 størrelse 2s 10,5X14,8 cm"); ?></option>
											<option value="a6_4s_4" <?php if($format_icc == "a6_4s_4"){ echo "selected"; } ?>><?php _e("A6 størrelse 4s 21X14,8 cm"); ?></option>
											<option value="SQ_2s_6" <?php if($format_icc == "SQ_2s_6"){ echo "selected"; } ?>><?php _e("SQ størrelse 2s 14X14 cm"); ?></option>
											<option value="SQ_4s_3" <?php if($format_icc == "SQ_4s_3"){ echo "selected"; } ?>><?php _e("SQ størrelse 4s 28X14 cm"); ?></option>
											<?php } ?>
										</select>
										<input type="hidden" value="<?php echo $id_of_imgs; ?>" name="product_option[<?php echo $product_option_row->option_id; ?>][row_value][<?php echo $product_option_row->id; ?>][drop_option_row_sort_order_id_of_imgs]" />
									</td>
									<?php
									if($data_wc_price_table != ""){
									?>
										<td class="datath3">
										<select name="product_option[<?php echo $product_option_row->option_id; ?>][row_value][<?php echo $product_option_row->id; ?>][drop_option_price_table_data]">
										<?php
											foreach($data_wc_price_table as $ptable){
												?><option <?php if($wp_wc_price_inmgg != "" && $wp_wc_price_inmgg == $ptable["id"]){ ?>selected<?php } ?> value="<?php echo $ptable["id"]; ?>"><?php echo $ptable["name"]; ?></option><?php
											}
										?>
										</select>
										</td>
									<?php
									}
									?>
									<td><a href="javascript:void(0)" onClick="deleteDropRow('<?php echo $product_option_row->id; ?>','<?php echo $product_option_row->option_id; ?>')">Remove</a></td>
								</tr>

							<?php } } ?>

								<tr>
									<td class="<?php echo $product_option->id; ?>droprowdata" colspan="5">
										<?php $custom_options->addForm2(0,$product_option->id); ?>
									</td>
								</tr>

							</tbody>
							<tfoot>
								<tr class="addButton">
							   		<td colspan="5"><input onClick="addNewDropRow(<?php echo $product_option->id; ?>)" type="button" id="btnAdd" class="button button-default" value="<?php echo _e('Add New Row','fmepco'); ?>"></td> 
							   	</tr>
							</tfoot>
							<tbody>
								
							</tbody>
						</table>
					</div>
					<div id="multiselect<?php echo $product_option->id; ?>" <?php if($product_option->option_field_type == 'multiple') { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
						<div class="rowfield_success<?php echo $product_option->id; ?>"></div>
						<table class="datatable" id="MultiTable<?php echo $product_option->id; ?>">
							<thead>
							    <tr>
							    	<th class="datath1"><label><b><?php echo _e('Title:','fmepco'); ?></b></label></th>
							    	<th class="datath2"><label><b><?php echo _e('Price:','fmepco'); ?></b></label></th>
							    	<th class="datath3"><label><b><?php echo _e('Price Type:','fmepco'); ?></b></label></th>
							    	<th class="datath4"><label><b><?php echo _e('Format icon:','fmepco'); ?></b></label></th>
							    	<th></th>
							    </tr>
							</thead>
							<tbody>
								<?php if($product_option->option_field_type == 'multiple') { ?>
								<?php foreach ($product_option_rows as $product_option_row) { ?>
								
									<tr id="tr<?php echo $product_option_row->id; ?>_<?php echo $product_option_row->option_id; ?>">
										<td class="datath1"><input class="inputs" type="text" value="<?php echo $product_option_row->option_row_title; ?>" name="product_option[<?php echo $product_option_row->option_id; ?>][row_value][<?php echo $product_option_row->id; ?>][multi_option_row_title]" id="dropdowntitle" /></td>
										<td class="datath2">
								    		<input class="inputs" type="text" value="<?php echo $product_option_row->option_row_price; ?>" name="product_option[<?php echo $product_option_row->option_id; ?>][row_value][<?php echo $product_option_row->id; ?>][multi_option_row_price]" id="price<?php echo $product_option_row->option_id; ?>__<?php echo $product_option_row->id; ?>" onChange="PriceOnly(this.id);" />
								        </td>
										<td class="datath3">
											<select class="select_is_required inputs" name="product_option[<?php echo $product_option_row->option_id; ?>][row_value][<?php echo $product_option_row->id; ?>][multi_option_row_price_type]" id="dropdownprice_type">
						                		<option value="fixed" <?php selected('fixed',$product_option_row->option_row_price_type); ?>><?php echo _e('Fixed','fmepco'); ?></option>
						                		<option value="percent" <?php selected('percent',$product_option_row->option_row_price_type); ?>><?php echo _e('Percent','fmepco'); ?></option>
						                	</select>
										</td>
										<td class="datath4"><input class="inputs" type="text" value="<?php echo $product_option_row->option_row_sort_order; ?>" name="product_option[<?php echo $product_option_row->option_id; ?>][row_value][<?php echo $product_option_row->id; ?>][multi_option_row_sort_order]" id="sort_order<?php echo $product_option_row->option_id; ?>__<?php echo $product_option_row->id; ?>" /></td>
										<td><a href="javascript:void(0)" onClick="deleteDropRow('<?php echo $product_option_row->id; ?>','<?php echo $product_option_row->option_id; ?>')">Remove</a></td>
									</tr>
								
								<?php } } ?>

								<tr>
									<td class="<?php echo $product_option->id; ?>multirowdata" colspan="5">
										<?php $custom_options->addMultiForm(0,$product_option->id); ?>
									</td>
								</tr>
							</tbody>
							<tfoot>
								<tr class="addButton">
							   		<td colspan="5"><input onClick="addNewMultiRow(<?php echo $product_option->id; ?>)" type="button" id="btnAdd" class="button button-default" value="<?php echo _e('Add New Row','fmepco'); ?>"></td> 
							   	</tr>
							</tfoot>
							<tbody>
								
							</tbody>
						</table>
					</div>
					
				</div>

			</div>

		<?php } } ?>

		<div class="tt">
			<?php $custom_options->addForm(0); ?>
		</div>

	</form>

</div>
					 

<script type="text/javascript">


function SortOrderonlyNumber(id){
	   /* var DataVal = document.getElementById(id).value;
	    if(!DataVal.match(/^[1-9][0-9]*$/)) {
	    	 alert('Only Integer values are allowed in Sort Order field!');
	    	 document.getElementById(id).value = DataVal.replace(/[^0-9]/g,'');
	    	 ('#'+id).focus();

	    }*/

	    
	}

	function MaxCharsonlyNumber(id){
	    var DataVal = document.getElementById(id).value;
	    if(!DataVal.match(/^[1-9][0-9]*$/)) {
	    	 alert('Only Integer values are allowed in Max Characters field!');
	    	 document.getElementById(id).value = DataVal.replace(/[^0-9]/g,'');
	    	 ('#'+id).focus();

	    }

	    
	}

	function PriceOnly(id){
	    var DataVal = document.getElementById(id).value;
	    if(!DataVal.match(/^[1-9.-][0-9.-]*$/)) {
	    	 alert('Only Numaric values are allowed in Price field!');
	    	 document.getElementById(id).value = DataVal.replace(/[^0-9.-]/g,'');
	    	 ('#'+id).focus();

	    }

	    
	}
	
	


	jQuery(document).ready(function($) { 
		
		
	   $('#field').toggle(function(){ 
		   $('#field_div').removeClass('ui-state-default widget').addClass('ui-state-default widget open');
		   $("#bw").slideDown('slow');
		   
	   },function(){
		$('#bw').removeClass('ui-state-default widget open').addClass('ui-state-default widget');
		   $("#bw").slideUp('slow');
	   });

	});

	function addFields() {

		var ajaxurl = "<?php echo admin_url( 'admin-ajax.php'); ?>";

		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: 'action=addoptionTempData',
			success: function(data) {
			   jQuery('.tt').append(data);
			}
		});
	}

	function addNewDropRow(field_id) {

		var ajaxurl = "<?php echo admin_url( 'admin-ajax.php'); ?>";

		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: {"action": "addrowTempData", "field_id":field_id},
			success: function(data) {
			   jQuery('.'+field_id+'droprowdata').append(data);
			}
		});

	}


	function addNewMultiRow(field_id) {

		var ajaxurl = "<?php echo admin_url( 'admin-ajax.php'); ?>";

		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: {"action": "addmultirowTempData", "field_id":field_id},
			success: function(data) {
			   jQuery('.'+field_id+'multirowdata').append(data);
			}
		});

	}
	function removeFormatsOneFieldThat(field_id, nr) {
            var element = jQuery(this);
		jQuery(".all_format_fields_here"+field_id).find(".one_format_in_all_formats_go"+nr).remove();
                jQuery(".all_format_fields_here"+field_id+" table.addFormats tr").eq(nr).remove();
	}
	function updateFormatFieldsGo(field_id) {
		var add_fields = [];
		jQuery("#field"+field_id).find("table.addFormats tbody tr").each(function(){
			var label_of_format = jQuery(this).find(".label_of_format").val();
			var file_url_all_here = jQuery(this).find(".file_url_all_here").val();
			var shadow_file_url_all_here = jQuery(this).find(".shadow_file_url_all_here").val();
                        var file2_url_all_here = jQuery(this).find(".file2_url_all_here").val();
			var shadow2_file_url_all_here = jQuery(this).find(".shadow2_file_url_all_here").val();
			if(label_of_format != "" && file_url_all_here != ""){
				add_fields.push({ label: label_of_format, file: file_url_all_here, shadow_file: shadow_file_url_all_here, file2: file2_url_all_here, shadow2_file: shadow2_file_url_all_here });
			}
		});
		
		var data = new FormData();
		var id_post = <?php echo $post->ID; ?>;
		for(var ii=0; ii < add_fields.length; ii++){
			var j=ii+1;
			data.append(j, add_fields[ii].label+"___FILE___"+add_fields[ii].file+"___FILE___"+add_fields[ii].shadow_file+"___FILE___"+add_fields[ii].file2+"___FILE___"+add_fields[ii].shadow2_file);
                        console.log(data);
			//data.append(j+"file", add_fields[ii].file);
		}
		data.append("post_id", id_post);
		
		var site_url = "<?php echo site_url(); ?>";
		jQuery.ajax({
			url: site_url+'/api/wp_wc_fma_custom_options_product?__api_wp_fma_custom_options_product_api=1&id=update_meta',
			type: 'POST',
			data: data,
			cache: false,
			dataType: 'json',
			processData: false,
			contentType: false,
			success: function(dataRet, textStatus, jqXHR)
			{
				location.reload();
			},
			error: function(jqXHR, textStatus, errorThrown)
			{
				console.log('ERRORS: ' + textStatus);
			}
		});
		
		
		
	}
        var newFormatsQty = 0;
	function addFormatFieldsGo(field_id) {
		var nr = 1;
                var labelId = field_id.toString() + newFormatsQty.toString();
		jQuery(".all_format_fields_here"+field_id).find(".one_format_in_all_formats").each(function(){
			var jq_data = parseInt(jQuery(this).attr("data-nr"));
			if(jq_data > nr){
				nr = jq_data;
			}
		});
		nr = nr+1;
		var format = '<tr><td>';
			 format += '<input type="text" class="label_of_format" name="label_of_one_format[]" placeholder="Format name">';
                         format += '</td><td>';
			format += '<input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" id="uploadIconInput'+labelId+'" class="file_of_format inputfile" name="pic_of_the_format[]" placeholder="Icon"><label for="uploadIconInput'+labelId+'" class="button">Upload icon</label>';
			 format += '<input type="hidden" class="file_url_all_here" name="file_url_all_here[]">';
                        format += '</td><td>';
                        format += '<input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" id="uploadShadowInput'+labelId+'" class="shadow_file_of_format inputfile" name="shadow_pic_of_the_format[]" placeholder="Shadow Icon"><label for="uploadShadowInput'+labelId+'" class="button">Upload shadow</label>';
                         format += '<input type="hidden" class="shadow_file_url_all_here" name="shadow_file_url_all_here[]">';
                        format += '</td><td>';
                        format += '<input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" id="uploadIcon2Input'+labelId+'" class="shadow_file_of_format inputfile" name="shadow_pic_of_the_format[]" placeholder="Shadow Icon"><label for="uploadIcon2Input'+labelId+'" class="button">Upload icon</label>';
                         format += '<input type="hidden" class="file2_url_all_here" name="file2_url_all_here[]">';
                        format += '</td><td>';
                        format += '<input onChange="prepareUploadForWpWcFMACustomOPtions(event)" type="file" id="uploadShadow2Input'+labelId+'" class="shadow_file_of_format inputfile" name="shadow_pic_of_the_format[]" placeholder="Shadow Icon"><label for="uploadShadow2Input'+labelId+'" class="button">Upload icon</label>';
                         format += '<input type="hidden" class="shadow2_file_url_all_here" name="shadow2_file_url_all_here[]">';
                        format += '</td><td>';
			format += '<input onClick="removeFormatsOneFieldThat('+field_id+','+nr+')" type="button" class="removethatOnefield button button-default" value="<?php echo _e('Remove format field', 'fmepco'); ?>">';
			format += '<div class="uploading_file_here"></div>';
			format += '</td></tr></div>';
                jQuery(".all_format_fields_here"+field_id+" .one_format_in_all_formats").last().after('<div data-nr="'+nr+'" class="one_format_in_all_formats one_format_in_all_formats_go'+nr+'">');
		jQuery(".all_format_fields_here"+field_id+" table.addFormats tbody").append(format);
                newFormatsQty++;
	}
	function delFields(field_id) {

		var ajaxurl = "<?php echo admin_url( 'admin-ajax.php'); ?>";
		if(confirm("Are you sure to delete this option? This action can not be undone."))
		{
			jQuery.ajax({
			type: "POST",
			url: ajaxurl,
			data: {"action": "deloptionTempData", "field_id":field_id},
			success: function() {

				jQuery("#field"+field_id).fadeOut('slow');
				jQuery("#field"+field_id).remove();

				jQuery('.field_success').html("<div class='updated notice alert'>Option Deleted Sucessfully!</div>");
				window.scrollTo(0, 0);
				
				jQuery('.alert').delay(5000).fadeOut('slow');

			}
			});

		}
	return false;
	}


	function deleteDropRow(id, field_id) {

		var ajaxurl = "<?php echo admin_url( 'admin-ajax.php'); ?>";
		if(confirm("Are you sure to delete this row? This action can not be undone."))
		{
			jQuery.ajax({
			type: "POST",
			url: ajaxurl,
			data: {"action": "delrowTempData", "field_id":field_id, "id":id},
			success: function() {

				jQuery("#tr"+id+"_"+field_id).fadeOut('slow');
				jQuery("#tr"+id+"_"+field_id).remove();
				jQuery('.rowfield_success'+field_id).html("<div id='message' class='updated notice alert'>Row Deleted Sucessfully!</div>");
				jQuery('.rowfield_success'+field_id+" #message").delay(5000).fadeOut('slow');

			}
			});

		}
	return false;
	}





	function showFields(field_id, value) {

		if(value == 'field') {

			jQuery('#textArea'+field_id).slideUp('slow');
			jQuery('#dropdown'+field_id).slideUp('slow');
			jQuery('#multiselect'+field_id).slideUp('slow');

			
			jQuery('#textField'+field_id).slideDown('slow');

		} else if(value == 'area') {

			jQuery('#textField'+field_id).slideUp('slow');
			jQuery('#dropdown'+field_id).slideUp('slow');
			jQuery('#multiselect'+field_id).slideUp('slow');

			
			jQuery('#textArea'+field_id).slideDown('slow');

		}  else if(value == 'drop_down') {

			jQuery('#textField'+field_id).slideUp('slow');
			jQuery('#textArea'+field_id).slideUp('slow');
			jQuery('#multiselect'+field_id).slideUp('slow');

			
			jQuery('#dropdown'+field_id).slideDown('slow');

		} else if(value == 'multiple') {

			jQuery('#textField'+field_id).slideUp('slow');
			jQuery('#textArea'+field_id).slideUp('slow');
			jQuery('#dropdown'+field_id).slideUp('slow');

			
			jQuery('#multiselect'+field_id).slideDown('slow');

		}  else {

			jQuery('#textField'+field_id).slideUp('slow');
			jQuery('#textArea'+field_id).slideUp('slow');
			jQuery('#dropdown'+field_id).slideUp('slow');
			jQuery('#multiselect'+field_id).slideUp('slow');

		}

	}
	


jQuery( document ).ready(function() {
	//jQuery('.one_format_in_all_formats input[type=file]').on('change', prepareUploadForWpWcFMACustomOPtions);
});
function prepareUploadForWpWcFMACustomOPtions(event)
{
	var elem = jQuery(event.target).parent();
        var element = jQuery(event.target);
        if(elem.find("label").length > 0) {
            elem.find("label").text("Uploading...");
        }
//        element.text("Uploading file...");
	elem.find(".uploading_file_here").text("Uploading file...");
        files = event.target.files;
        event.stopPropagation(); // Stop stuff happening
        event.preventDefault(); // Totally stop stuff happening
	//jQuery(".file_upload_for_wp_wc_price_table_upload_container").hide();
	//jQuery(".file_upload_for_wp_wc_price_table_upload_loading_ing").show();
    var data = new FormData();
    jQuery.each(files, function(key, value)
    {
        data.append(key, value);
    });
	var id_post = <?php echo $post->ID; ?>;//parseInt(jQuery("#wp_wc_price_table_upload_file_id_of_post").html());
	var site_url = "<?php echo site_url(); ?>";
	jQuery.ajax({
        url: site_url+'/api/wp_wc_fma_custom_options_product?__api_wp_fma_custom_options_product_api=1&id=image_add',
        type: 'POST',
        data: data,
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function(dataRet, textStatus, jqXHR) {
			//jQuery(".file_upload_for_wp_wc_price_table_upload_container").show();
			//jQuery(".file_upload_for_wp_wc_price_table_upload_loading_ing").hide();
            if(dataRet.name == "data"){
                var retJson = JSON.parse(dataRet.value);
                var file_url = retJson[0];
                if(elem.find("input:hidden").length > 0) {
                    elem.find("input:hidden").val(file_url);
                }
//                if(jQuery(element).hasClass("file_of_format")) {
//                    elem.find(".file_url_all_here").val(file_url);
//                }   else if (jQuery(element).hasClass("shadow_file_of_format")) {
//                    elem.find(".shadow_file_url_all_here").val(file_url);
//                }
                
                if(elem.find("label").length > 0) {
                    elem.find("label").removeClass("button").addClass("inputclass").html('<img src="../wp-content/uploads'+file_url+'" class="adminFormatIcon"><img src="../wp-content/uploads'+file_url+'" class="enlarge">');
                } else {
                    element.replaceWith('<img src="../wp-content/uploads'+file_url+'" class="adminFormatIcon"> ');
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
			elem.find(".uploading_file_here").text("Error uploading " + textStatus);
//                        element.text("Error uploading " + textStatus);
            console.log('ERRORS: ' + textStatus);
        }
    });
}

</script>
