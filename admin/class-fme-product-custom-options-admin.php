<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


if ( !class_exists( 'FME_Product_Custom_Options_Admin' ) ) { 

	class FME_Product_Custom_Options_Admin extends FME_Product_Custom_Options {

		public function __construct() {

			add_action( 'wp_loaded', array( $this, 'admin_init' ) );
			//$this->module_settings = $this->get_module_settings();
			
			add_action( 'add_meta_boxes', array($this, 'fme_product_custom_options_box' ));
			add_action('save_post', array($this, 'fme_save_product_meta'), 1, 2);
			add_action('wp_ajax_addoptionTempData', array($this, 'addoptionTemData'));
			add_action('wp_ajax_deloptionTempData', array($this, 'deloptionTempData'));
			add_action('deloptionTempData', array($this, 'deloptionTempData'));
			add_action('wp_ajax_addrowTempData', array($this, 'addrowTemData'));
			add_action('wp_ajax_addmultirowTempData', array($this, 'addmultirowTemData'));
			add_action('wp_ajax_delrowTempData', array($this, 'delrowTempData'));
			add_action('delrowTempData', array($this, 'delrowTempData'));
			add_action('chili_delete_to_product_custom_format_option_fma', array($this, 'deleteThatInfoFormatHereForChili'));
			add_action('chili_delete_to_product_custom_format_delete_opt_one', array($this, 'deleteThatInfoFormatHereForChiliOneFormatOnly'));
			add_action('chili_add_to_product_custom_format_option_fma', array($this, 'addInfoForFormatOrDelForIt'));
			add_action('wp_ajax_chili_reimport_to_product_custom_format', array($this, 'reimportFormatOnGoGo'));

			
			add_filter('woocommerce_placeholder_img_src', array($this, 'custom_woocommerce_placeholder_img_src'));
			
		}

	public function woocommerce_get_custom_item_price_formats_arr($prod_id, $format_name){
		
		$data_go = "";
		$arr_return = array();
		$tabledata = $this->getPostTableDataWithAdvanced($prod_id,true);
		for($iij=0; $iij < count($tabledata);$iij++){
			$obj = $tabledata[$iij];
			if(is_object($obj)){
				$quantities = $obj->quantities;
				$arr_vars = $obj->vars;
				for($i=0; $i < count($arr_vars); $i++){
					$ar_vars_obj = $arr_vars[$i];
					$id = $ar_vars_obj->id;// use this
					$name = $ar_vars_obj->name;
					$arr_return[] = array("id" => $id, "name"=>$name);
				}
			}
		}
		return $arr_return;
	}
	public function getPostTableDataWithAdvanced($postId,$getTable = false,$onlyTable = false,$selected_paper = null){
		$selected_table = get_post_meta($postId,"_wp-wc-price-table");
		
		if(count($selected_table)){
			global $wpdb;
			$ret = array("postid"=>$postId);
			if($getTable){
			
			
				$table = $wpdb->get_results( 
					"
					SELECT *
					FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["tables"]." 
					WHERE id = ".$selected_table[0]."
					"
				);
				$tables = $table;
                                
				
                                if(count($table) == 0 || !isset($table[0]->id))
                                    return null;//table is not selected or does not exist
								
                foreach ($tables as $table){
					$vars = $wpdb->get_results( 
						"
						SELECT * 
						FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["variables"]." 
							WHERE table_id=".$table->id."
						"
					);
					if(count($vars) > 0){
						
						foreach($vars as $var){
							$opts = $wpdb->get_results( 
								"
								SELECT * 
								FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["options"]." o
									WHERE var_id=".$var->id."
								"
							);

							if(count($opts) > 0){
							   
								foreach($opts as $opt){

									$opts_a = $wpdb->get_results(
											"SELECT * "
											." FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["options_advanced"]." oa"
											." WHERE oa.opt_id=".$opt->id." AND oa.role=1"
											);
									if(count($opts_a)){
										$opt->options_advanced = $opts_a;
									}
								}
								
								$var->options = $opts;
							}
						}
						
						$table->vars = $vars;
					}
				}
				return $tables;
				
				$ret["table"] = $table;
				$ret["table"][0]->quantities = explode(",",$ret["table"][0]->quantities);
				//$ret["table"][0]->shippings = explode(",",$ret["table"][0]->shippings);
				
				if($onlyTable)
					return $ret;
			}
			if($selected_paper == null){
                            $selected_paper = get_post_meta($postId,"_".$this->plugin_name.'_paper');
							if(isset($selected_paper[0])){
								$selected_paper = $selected_paper[0];
							}else{
								$selected_paper = "";
							}
                        }
                        else{
                            //nothing to do
                        }
			if($selected_paper != ""){
				$paper = $wpdb->get_results( 
					"SELECT *
					FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["paper"]." WHERE id=".$selected_paper." LIMIT 1");
				
				if(isset($paper[0])){
					$ret["paper"] = $paper[0];
				}
			}
			$vars = $wpdb->get_results( 
				"
				SELECT *
				FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["variables"]." 
				WHERE table_id = ".$selected_table[0]."
				ORDER BY ord asc
				"
			);
			
			foreach ( $vars as $var ){
				
				if($var->type == "paper" || $var->type == "coverpaper"){
					
					$var->options = $wpdb->get_results( 
						"
						SELECT p.id,CONCAT(p.tykkelse,'".__("gr. ",'wp-wc-price-table')."',p.name) as name, p.xmlname, o.default,p.infopost
						FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["paperprices"]." p
						LEFT JOIN ".$wpdb->prefix . wp_wc_price_table::$data_tables["options"]." o
						ON p.id = o.charge and o.name='paper' and o.var_id=".$var->id."
						WHERE o.weight=1
						ORDER BY o.ord asc
						"
					);
					
					//print_r($var->options);
					
				}
				else{
				
					$var->options = $wpdb->get_results( 
						"
						SELECT *
						FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["options"]." 
						WHERE var_id = ".$var->id."
						ORDER BY ord asc
						"
					);
				}
			}
			
			$ret["vars"] = $vars;
			
			return $ret;
		}
		else return false;
	}
		
                public function custom_woocommerce_placeholder_img_src( $src ) {
			$upload_dir = wp_upload_dir();
			$uploads = untrailingslashit( $upload_dir['baseurl'] );
			//$src = $uploads . '/placeholder.png';
			global $post;
			$meta_info_previews = get_post_meta($post->ID, 'previews_for_chili', true);
			$arr_of_previews = array();
			if($meta_info_previews != ""){
				$objdec = json_decode($meta_info_previews);
				if($objdec != null){
					$arr_of_previews = (array)$objdec;
                                        
                                        //print_r($arr_of_previews);
                                        if(count($arr_of_previews)){
                                            $src = explode(";", $arr_of_previews[0]->previews)[1];
                                            $src = str_replace("http://", "//", $src);
                                        }
				}
			}
			/*$ret_imgs_src = $this->getCustomOptionsOnHere($arr_of_previews);
			if($ret_imgs_src != ""){
				$src = $ret_imgs_src;
			}*/

			return $src;
		}

		public function admin_init() {
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );	
		}

		public function admin_scripts() {	
           
        	wp_enqueue_style( 'fmepco-admin-css', plugins_url( '/css/fmepco_style.css', __FILE__ ), false );
        	
        }

        

		

		function fme_product_custom_options_box() {
    		add_meta_box( 'product_custom_optios', 'Product Custom Options', array($this, 'product_custom_options_call'), 'product', 'normal', 'high' );

		}

		function fme_save_product_meta($post_id, $post) { 

			  
			if ( !current_user_can( apply_filters( 'fmepco_capability', 'manage_options' ) ) )
			die( '-1' );



			global $wpdb;
			if(isset($_POST['editpro']) && $_POST['editpro'] == 'yes') {
				$wpdb->query( $wpdb->prepare( "DELETE FROM ".$wpdb->fmepco_poptions_table." WHERE product_id = %d", $post->ID) );

			}
			//echo "<pre>";
			//print_r($_POST['product_option']);exit();
			if(count($_POST['product_option'])!=0) {
			foreach ($_POST['product_option'] as $product_option) {

				if(isset($product_option['option_id']) && $product_option['option_id']!='') {
					$opt_id = $product_option['option_id'];
				} else { $opt_id = 0; }

				if(isset($_POST['editpro']) && $_POST['editpro'] == 'yes') {
					$wpdb->query( $wpdb->prepare( "DELETE FROM ".$wpdb->fmepco_rowoption_table." WHERE option_id = %d", intval($opt_id)) );
				}

				

				if(isset($product_option['option_type']) && $product_option['option_type'] == 'field') {

					if($product_option['option_title'] != '') {
						$wpdb->query($wpdb->prepare( 
			            "
			            INSERT INTO $wpdb->fmepco_poptions_table
			            (product_id,option_title,option_field_type,option_is_required,option_sort_order,option_price,option_price_type,option_maxchars)
			            VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
			            ",
			            $post->ID,
			            sanitize_text_field($product_option['option_title']),
			            sanitize_text_field($product_option['option_type']),
			            sanitize_text_field($product_option['option_is_required']),
			            sanitize_text_field($product_option['option_sort_order']),
			            sanitize_text_field($product_option['text_option_price']),
			            sanitize_text_field($product_option['text_option_price_type']),
			            sanitize_text_field($product_option['text_option_maxchars'])
			            ) );
					}
				} else if(isset($product_option['option_type']) && $product_option['option_type'] == 'area') {

					if(sanitize_text_field($product_option['option_title']) != '') {
						$wpdb->query($wpdb->prepare( 
			            "
			            INSERT INTO $wpdb->fmepco_poptions_table
			            (product_id,option_title,option_field_type,option_is_required,option_sort_order,option_price,option_price_type,option_maxchars)
			            VALUES (%s,%s,%s,%s,%s,%s,%s,%s)
			            ",
			            $post->ID,
			            sanitize_text_field($product_option['option_title']),
			            sanitize_text_field($product_option['option_type']),
			            sanitize_text_field($product_option['option_is_required']),
			            sanitize_text_field($product_option['option_sort_order']),
			            sanitize_text_field($product_option['area_option_price']),
			            sanitize_text_field($product_option['area_option_price_type']),
			            sanitize_text_field( $product_option['area_option_maxchars'])
			            ) );
					}
				} else if(isset($product_option['option_type']) && $product_option['option_type'] == 'drop_down') {

					if(sanitize_text_field($product_option['option_title']) != '') {
						$wpdb->query($wpdb->prepare( 
			            "
			            INSERT INTO $wpdb->fmepco_poptions_table
			            (product_id,option_title,option_field_type,option_is_required,option_sort_order)
			            VALUES (%s,%s,%s,%s,%s)
			            ",
			            $post->ID,
			            sanitize_text_field($product_option['option_title']),
			            sanitize_text_field($product_option['option_type']),
			            sanitize_text_field($product_option['option_is_required']),
			            sanitize_text_field($product_option['option_sort_order'])
			            ));
			            $lastid = $wpdb->insert_id;

						foreach ($product_option['row_value'] as $row_value) {
							
							if(sanitize_text_field($row_value['drop_option_row_title']) != '') {
								$drop_row_opt_faked = $row_value['drop_option_row_title_faked_one'];
								$drop_row_opt = $row_value['drop_option_row_title'];
								if(isset($row_value['drop_option_row_title_faked_one']) && $row_value['drop_option_row_title_faked_one'] != ""){
									$drop_row_opt_faked = $row_value['drop_option_row_title_faked_one'];
									$drop_row_opt = $drop_row_opt_faked;
									if(isset($displayValue) && strpos($displayValue, "alternative_chili_id") !== FALSE){
										$drop_row_opt = $drop_row_opt_faked."alternative_chili_id";
									}
									//if($drop_row_opt)
								}
							$rotation_data = "";
							$folding_data = "";
							$wp_wc_price_data = "";//drop_option_price_table_data
								if(isset($row_value['drop_option_row_rotate'])){
									$rotation_data = $row_value['drop_option_row_rotate'];
								}
								if(isset($row_value['drop_option_row_folding']) && $row_value['drop_option_row_folding'] != ""){
									$folding_data = $row_value['drop_option_row_folding'];
								}
								if(isset($row_value['drop_option_price_table_data']) && $row_value['drop_option_price_table_data'] != ""){
									$wp_wc_price_data = $row_value['drop_option_price_table_data'];
								}
								
								$wpdb->query($wpdb->prepare( 
					            "
					            INSERT INTO $wpdb->fmepco_rowoption_table
					            (option_id,option_row_title,option_row_price,option_row_price_type,option_row_sort_order)
					            VALUES (%s,%s,%s,%s,%s)
					            ",
					            $lastid,
					            $drop_row_opt,
					            sanitize_text_field($row_value['drop_option_row_price']),
					            sanitize_text_field($row_value['drop_option_row_price_type']),
					            sanitize_text_field($row_value['drop_option_row_sort_order'].";".$row_value['drop_option_row_sort_order_id_of_imgs'].";".$rotation_data.";".$folding_data.";".$wp_wc_price_data)
					            ));
							}
							
						}
					}
					

				} else if(isset($product_option['option_type']) && $product_option['option_type'] == 'multiple') {

					if(sanitize_text_field($product_option['option_title']) != '') {
						$wpdb->query($wpdb->prepare( 
			            "
			            INSERT INTO $wpdb->fmepco_poptions_table
			            (product_id,option_title,option_field_type,option_is_required,option_sort_order)
			            VALUES (%s,%s,%s,%s,%s)
			            ",
			            $post->ID,
			            sanitize_text_field($product_option['option_title']),
			            sanitize_text_field($product_option['option_type']),
			            sanitize_text_field($product_option['option_is_required']),
			            sanitize_text_field($product_option['option_sort_order'])
			            ));
			            $lastid = $wpdb->insert_id;

						foreach ($product_option['row_value'] as $row_value) {
							
							if(sanitize_text_field($row_value['multi_option_row_title']) != '') {
								$wpdb->query($wpdb->prepare( 
					            "
					            INSERT INTO $wpdb->fmepco_rowoption_table
					            (option_id,option_row_title,option_row_price,option_row_price_type,option_row_sort_order)
					            VALUES (%s,%s,%s,%s,%s)
					            ",
					            $lastid,
					            $row_value['multi_option_row_title'],
					            sanitize_text_field($row_value['multi_option_row_price']),
					            sanitize_text_field($row_value['multi_option_row_price_type']),
					            sanitize_text_field($row_value['multi_option_row_sort_order'])
					            ));
							}
						}
					}

					

				}  

			} 
			
			}
			
	    	
		}
		function deleteThatAllFormatsHereForChiliFormatsOnly($pid){
			//$pid = $dat["pid"];
			//$format = $dat["remove"];
			$option_title_chili = "format for chili selection";
			$get_existing_row = $this->getFieldByCustomField("option_title", $option_title_chili, $pid);
			if($get_existing_row != null){
				$lastid = $get_existing_row->id;
				$this->deleteProductOptionRowsAllThere($lastid);
				//$this->deleteOptionalFormatHere($lastid);
				//delrowTempDataFieldOnly($id, $field_id)
			}
		}
		function deleteThatInfoFormatHereForChiliOneFormatOnly($dat){
			$pid = $dat["pid"];
			$format = $dat["remove"];
			$option_title_chili = "format for chili selection";
			$get_existing_row = $this->getFieldByCustomField("option_title", $option_title_chili, $pid);
			if($get_existing_row != null){
				$lastid = $get_existing_row->id;
				$this->deleteProductOptionRows($lastid, $format);
				//$this->deleteOptionalFormatHere($lastid);
				//delrowTempDataFieldOnly($id, $field_id)
			}
		}
		
		function deleteThatInfoFormatHereForChili($pid){
			$option_title_chili = "format for chili selection";
			$get_existing_row = $this->getFieldByCustomField("option_title", $option_title_chili, $pid);
			if($get_existing_row != null){
				$lastid = $get_existing_row->id;
				$this->deleteOptionalFormatHere($lastid);
			}
		}
		function deleteOptionalFormatHere($id) {
			global $wpdb;
			$wpdb->query( $wpdb->prepare( "DELETE FROM ".$wpdb->fmepco_poptions_table . " WHERE id = %d", $id ) );
			return true;
		}
		function reimportFormatOnGoGo(){
			$pid = $_POST["data"]["pid"];
			$formats = $_POST["data"]["formats"];
			$this->deleteThatAllFormatsHereForChiliFormatsOnly($pid);
			$formatss_bad = explode(";", $formats);
			$formatss = array();
			for($ii=0; $ii < count($formatss_bad); $ii++){
				if(!in_array($formatss_bad[$ii], $formatss)){
					$formatss[] = $formatss_bad[$ii];
				}
			}
			for($ii=0; $ii < count($formatss); $ii++){
				if($formatss[$ii] != ""){
					$data = $this->getInfoOfPreviewsFormats($pid, $formatss[$ii]);
					if(isset($data["displayValue"])){
						$formatt = $formatss[$ii];
						$format_add["displayValue"] = $data["displayValue"];
						$format_add["id"] = $formatt;
						$format_add["pid"] = $pid;
						$this->addInfoForFormatOrDelForIt($format_add);
					}
				}
			}
		}
		function getInfoOfPreviewsFormats($pid, $idd_format){
			$arr = array();
			$post_get_meta = get_post_meta($pid, 'previews_for_chili', true);
			if($post_get_meta != ""){
				$objdec = json_decode($post_get_meta);
				if($objdec != null){
					$objdec = (array)$objdec;
					if(count($objdec) > 0){
						for($ii=0; $ii < count($objdec); $ii++){
							$objdec_id = $objdec[$ii]->id;
							$format_value = $objdec[$ii]->format_value;
							$displayValue = $objdec[$ii]->displayValue;
							//$height = $objdec[$ii]->height;
							//$width = $objdec[$ii]->width;
							$objdec_previews = $objdec[$ii]->previews;
							if($idd_format == $objdec_id || $format_value == $idd_format){
								$arr["displayValue"] = $displayValue;
							}
						}
					}
				}
			}
			return $arr;
		}
		function addInfoForFormatOrDelForIt($format_add){
			global $wpdb;
			$pid = $format_add["pid"];
			$option_title_chili = "format for chili selection";
			$get_existing_row = $this->getFieldByCustomField("option_title", $option_title_chili, $pid);
			$lastid = "";
			if($get_existing_row == null){
					$wpdb->query($wpdb->prepare( 
			            "
			            INSERT INTO $wpdb->fmepco_poptions_table
			            (product_id,option_title,option_field_type,option_is_required,option_sort_order)
			            VALUES (%s,%s,%s,%s,%s)
			            ",
			            $pid,
			            sanitize_text_field($option_title_chili),
			            sanitize_text_field("drop_down"),
			            sanitize_text_field("yes"),
			            sanitize_text_field("")
			            ));
			            $lastid = $wpdb->insert_id;
						
			}else{
				$lastid = $get_existing_row->id;
			}
			
							if($lastid != "" && sanitize_text_field($format_add["displayValue"]) != '') {
								$wpdb->query($wpdb->prepare( 
					            "
					            INSERT INTO $wpdb->fmepco_rowoption_table
					            (option_id,option_row_title,option_row_price,option_row_price_type,option_row_sort_order)
					            VALUES (%s,%s,%s,%s,%s)
					            ",
					            $lastid,
					            $format_add['displayValue'],
					            sanitize_text_field("10"),
					            sanitize_text_field("fixed"),
					            sanitize_text_field("a5_2s_1;".$format_add['id'])
					            ));
							}
			
			
				/*if(isset($product_option['option_type']) && $product_option['option_type'] == 'drop_down') {

					if(sanitize_text_field($product_option['option_title']) != '') {
						$wpdb->query($wpdb->prepare( 
			            "
			            INSERT INTO $wpdb->fmepco_poptions_table
			            (product_id,option_title,option_field_type,option_is_required,option_sort_order)
			            VALUES (%s,%s,%s,%s,%s)
			            ",
			            $post->ID,
			            sanitize_text_field($product_option['option_title']),
			            sanitize_text_field($product_option['option_type']),
			            sanitize_text_field($product_option['option_is_required']),
			            sanitize_text_field($product_option['option_sort_order'])
			            ));
			            $lastid = $wpdb->insert_id;

						foreach ($product_option['row_value'] as $row_value) {
							
							if(sanitize_text_field($row_value['drop_option_row_title']) != '') {
								$wpdb->query($wpdb->prepare( 
					            "
					            INSERT INTO $wpdb->fmepco_rowoption_table
					            (option_id,option_row_title,option_row_price,option_row_price_type,option_row_sort_order)
					            VALUES (%s,%s,%s,%s,%s)
					            ",
					            $lastid,
					            sanitize_text_field($row_value['drop_option_row_title']),
					            sanitize_text_field($row_value['drop_option_row_price']),
					            sanitize_text_field($row_value['drop_option_row_price_type']),
					            sanitize_text_field($row_value['drop_option_row_sort_order'].";".$row_value['drop_option_row_sort_order_id_of_imgs'])
					            ));
							}
							
						}
					}
					

				}*/
		}
		function getFieldByCustomField($field, $field_value, $pid) { 
			global $wpdb;
            $result = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM ".$wpdb->fmepco_poptions_table." WHERE product_id = ".$pid." AND ".$field." = %s", $field_value));      
            return $result;
		}
		function product_custom_options_call( $post ) { 

			global $wpdb;
			$wpdb->query("TRUNCATE TABLE ".$wpdb->fmepco_temp_table);

			require  FMEPCO_PLUGIN_DIR . 'admin/view/product_level_form.php';
		}
		function getTempFields($id) { 
			global $wpdb;
            $result = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM ".$wpdb->fmepco_temp_table." WHERE field_type = %s AND  id = %d", 'option', $id));      
            return $result;
		}

		function getrowTempFields($id,$field_id) {
			global $wpdb;

            $result = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM ".$wpdb->fmepco_temp_table." WHERE field_type = %s AND field_id = %d AND id = %d", 'row', $field_id, $id));      
            return $result;
		}

		function getProductOptions($post) { 

			global $wpdb;
			
            $result = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM ".$wpdb->fmepco_poptions_table." WHERE product_id = %d",$post));      
            return $result;
		}

		function deleteProductOptionRows($opt_id, $row_string) {
			global $wpdb;
					$data = $wpdb->query("DELETE FROM `".$wpdb->fmepco_rowoption_table."` WHERE option_id = ".$opt_id." AND option_row_sort_order LIKE '%".$row_string."%'");
		}
		function deleteProductOptionRowsAllThere($opt_id) {
			global $wpdb;
					$data = $wpdb->query("DELETE FROM `".$wpdb->fmepco_rowoption_table."` WHERE option_id = ".$opt_id."");
		}
		function getProductOptionRows($option_id) { 

			global $wpdb;
			
            $result = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM ".$wpdb->fmepco_rowoption_table." WHERE option_id = %d",$option_id));      
            return $result;
		}

		function addForm($id) { ?>

			<?php 

		   		$tempField = $this->getTempFields($id);
		   		if(count($tempField)!=0) {
		   		
		   			
		   	?>

			<div class="addFormFields" id="field<?php echo $tempField->id; ?>">
				<input onClick="delFields('<?php echo $tempField->id; ?>')" type="button" class="btnDel button btn-danger button-large" value="<?php echo _e('Delete Option','fmepco'); ?>">
				<div class="topFields">
					<table class="datatable">
						<thead>
						    <tr>
						    	<th class="datath1"><label><b><?php echo _e('Title:','fmepco'); ?></b></label></th>
						    	<th class="datath2"><label><b><?php echo _e('Input Type:','fmepco'); ?></b></label></th>
						    	<th class="datath3"><label><b><?php echo _e('Is Required:','fmepco'); ?></b></label></th>
						    	<th class="datath4"><label><b>Sort Order:</b></label></th>
						    	</tr>
						</thead>
						<tbody>
							<tr>
								<td class="datath1"><input class="inputs" type="text" name="product_option[<?php echo $tempField->id; ?>][option_title]" id="title" /></td>
								<td class="datath2">
						    		<select class="select_type inputs" name="product_option[<?php echo $tempField->id; ?>][option_type]" id="type" onChange="showFields('<?php echo $tempField->id; ?>',this.value)">
						    			<option value=""><?php echo _e('-- Please select --','fmepco'); ?></option>
						    			<optgroup label="Text">
						    				<option value="field"><?php echo _e('Field','fmepco'); ?></option>
						    				<option value="area"><?php echo _e('Area','fmepco'); ?></option>
						    			</optgroup>
						    			
						    			<optgroup label="Select">
						    				<option value="drop_down"><?php echo _e('Drop-down','fmepco'); ?></option>
			            					<option value="multiple"><?php echo _e('Multiple Select','fmepco'); ?></option>
			            				</optgroup>
					            		
			            			</select>
						        </td>
								<td class="datath3">
									<select class="select_is_required inputs" name="product_option[<?php echo $tempField->id; ?>][option_is_required]" id="is_required">
				                		<option value="yes"><?php echo _e('Yes','fmepco'); ?></option>
				                		<option value="no"><?php echo _e('No','fmepco'); ?></option>
				                	</select>
								</td>
								<td class="datath4"><input class="inputs" type="text" name="product_option[<?php echo $tempField->id; ?>][option_sort_order]" id="sort_order<?php echo $tempField->id; ?>" onChange="SortOrderonlyNumber(this.id);" /></td>
							</tr>
							
						</tbody>
					</table>
				</div>

				<div class="bottom_fields">
					<div id="textField<?php echo $tempField->id; ?>" style="display:none;">
						<table class="datatable">
							<thead>
							    <tr>
							    	<th class="datath1"><label><b><?php echo _e('Price:','fmepco'); ?></b></label></th>
							    	<th class="datath2"><label><b><?php echo _e('Price Type:','fmepco'); ?></b></label></th>
							    	<th class="datath3"><label><b><?php echo _e('Max Characters:','fmepco'); ?></b></label></th>
							    </tr>
							</thead>
							<tbody>
								<tr>
									<td class="datath1"><input class="inputs" type="text" name="product_option[<?php echo $tempField->id; ?>][text_option_price]" id="price<?php echo $tempField->id; ?>" onChange="PriceOnly(this.id);" /></td>
									<td class="datath2">
										<select class="select_is_required inputs" name="product_option[<?php echo $tempField->id; ?>][text_option_price_type]" id="fieldprice_type">
					                		<option value="fixed"><?php echo _e('Fixed','fmepco'); ?></option>
					                		<option value="percent"><?php echo _e('Percent','fmepco'); ?></option>
					                	</select>
									</td>
									<td class="datath3"><input class="inputs" type="text" name="product_option[<?php echo $tempField->id; ?>][text_option_maxchars]" id="maxchars<?php echo $tempField->id; ?>" onChange="MaxCharsOnlyNumber(this.id);" /></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div id="textArea<?php echo $tempField->id; ?>" style="display:none;">
						<table class="datatable">
							<thead>
							    <tr>
							    	<th class="datath1"><label><b><?php echo _e('Price:','fmepco'); ?></b></label></th>
							    	<th class="datath2"><label><b><?php echo _e('Price Type:','fmepco'); ?></b></label></th>
							    	<th class="datath3"><label><b><?php echo _e('Max Characters:','fmepco'); ?></b></label></th>
							    </tr>
							</thead>
							<tbody>
								<tr>
									<td class="datath1"><input class="inputs" type="text" name="product_option[<?php echo $tempField->id; ?>][area_option_price]" id="price<?php echo $tempField->id; ?>" onChange="PriceOnly(this.id);" /></td>
									<td class="datath2">
										<select class="select_is_required inputs" name="product_option[<?php echo $tempField->id; ?>][area_option_price_type]" id="areaprice_type">
					                		<option value="fixed"><?php echo _e('Fixed','fmepco'); ?></option>
					                		<option value="percent"><?php echo _e('Percent','fmepco'); ?></option>
					                	</select>
									</td>
									<td class="datath3"><input class="inputs" type="text" name="product_option[<?php echo $tempField->id; ?>][area_option_maxchars]" id="maxchars<?php echo $tempField->id; ?>" onChange="SMaxCharsonlyNumber(this.id);" /></td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div id="dropdown<?php echo $tempField->id; ?>" style="display:none;">
						<div class="rowfield_success<?php echo $tempField->id; ?>"></div>
						<table class="datatable" id="POITable<?php echo $tempField->id; ?>">
							<thead>
							    <tr>
							    	<th class="datath1"><label><b><?php echo _e('Title:','fmepco'); ?></b></label></th>
							    	<th class="datath2"><label><b><?php echo _e('Price:','fmepco'); ?></b></label></th>
							    	<th class="datath3"><label><b><?php echo _e('Price Type:','fmepco'); ?></b></label></th>
							    	<th class="datath4"><label><b><?php echo _e('Format icon:','fmepco'); ?></b></label></th>
							    	<th></th>
							    </tr>
							</thead>
							<tbody>

								
								<tr>
									<td class="<?php echo $tempField->id; ?>droprowdata" colspan="4"></td>
								</tr>
							</tbody>
							<tfoot>
								<tr class="addButton">
							   		<td colspan="5"><input onClick="addNewDropRow(<?php echo $tempField->id; ?>)" type="button" id="btnAdd" class="button button-default" value="<?php echo _e('Add New Row','fmepco'); ?>"></td> 
							   	</tr>
							</tfoot>
							<tbody>
								
							</tbody>
						</table>
					</div>
					<div id="multiselect<?php echo $tempField->id; ?>" style="display:none;">
						<div class="rowfield_success<?php echo $tempField->id; ?>"></div>
						<table class="datatable" id="MultiTable<?php echo $tempField->id; ?>">
							<thead>
							    <tr>
							    	<th class="datath1"><label><b><?php echo _e('Title:','fmepco'); ?></b></label></th>
							    	<th class="datath2"><label><b><?php echo _e('Price:','fmepco'); ?></b></label></th>
							    	<th class="datath3"><label><b><?php echo _e('Price Type:','fmepco'); ?></b></label></th>
							    	<th class="datath4"><label><b><?php echo _e('Format icon:','fmepco'); ?></b></label></th>
							    	<th></th>
							    </tr>
							</thead>
							<tbody>
								
								<tr>
									<td class="<?php echo $tempField->id; ?>multirowdata" colspan="4"></td>
								</tr>
							</tbody>
							<tfoot>
								<tr class="addButton">
							   		<td colspan="5"><input onClick="addNewMultiRow(<?php echo $tempField->id; ?>)" type="button" id="btnAdd" class="button button-default" value="<?php echo _e('Add New Row','fmepco'); ?>"></td> 
							   	</tr>
							</tfoot>
							<tbody>
								
							</tbody>
						</table>
					</div>
					
					
				</div>

			</div>
			<?php }  ?>

		<?php }

		function addForm2($id,$field_id) { ?>

			<?php 

		   		$tempField = $this->getrowTempFields($id,$field_id);
		   		if(count($tempField)!=0) {
		   		
		   			
		   	?>

		   		<tr id="tr<?php echo $tempField->id; ?>_<?php echo $tempField->field_id; ?>">
					<td class="datath1"><input class="inputs" type="text" name="product_option[<?php echo $tempField->field_id; ?>][row_value][<?php echo $tempField->id; ?>][drop_option_row_title]" id="dropdowntitle" /></td>
					<td class="datath2">
			    		<input class="inputs" type="text" name="product_option[<?php echo $tempField->field_id; ?>][row_value][<?php echo $tempField->id; ?>][drop_option_row_price]" id="price<?php echo $tempField->field_id; ?>__<?php echo $tempField->id; ?>" onChange="PriceOnly(this.id);" />
			        </td>
					<td class="datath3">
						<select class="select_is_required inputs" name="product_option[<?php echo $tempField->field_id; ?>][row_value][<?php echo $tempField->id; ?>][drop_option_row_price_type]" id="dropdownprice_type">
	                		<option value="fixed"><?php echo _e('Fixed','fmepco'); ?></option>
	                		<option value="percent"><?php echo _e('Percent','fmepco'); ?></option>
	                	</select>
					</td>
					<td class="datath4">
						<select name="product_option[<?php echo $tempField->field_id; ?>][row_value][<?php echo $tempField->id; ?>][drop_option_row_sort_order]" id="sort_order<?php echo $tempField->field_id; ?>__<?php echo $tempField->id; ?>" >
							<option value="a5_2s_1"><?php _e("A5 størrelse 2s 14,8X21 cm"); ?></option>
							<option value="a5_4s_2"><?php _e("A5 størrelse 4s 29,7X21 cm"); ?></option>
							<option value="a6_2s_8"><?php _e("A6 størrelse 2s 10,5X14,8 cm"); ?></option>
							<option value="a6_4s_4"><?php _e("A6 størrelse 4s 21X14,8 cm"); ?></option>
							<option value="SQ_2s_6"><?php _e("SQ størrelse 2s 14X14 cm"); ?></option>
							<option value="SQ_4s_3"><?php _e("SQ størrelse 4s 28X14 cm"); ?></option>
						</select>
						<input type="text" value="" name="product_option[<?php echo $tempField->field_id; ?>][row_value][<?php echo $tempField->id; ?>][drop_option_row_sort_order_id_of_imgs]" />
					
					
					
					<?php /*<input class="inputs" type="text" name="product_option[<?php echo $tempField->field_id; ?>][row_value][<?php echo $tempField->id; ?>][drop_option_row_sort_order]" id="sort_order<?php echo $tempField->field_id; ?>__<?php echo $tempField->id; ?>" onChange="SortOrderonlyNumber(this.id);" /> */?>
					</td>
					<td><a href="javascript:void(0)" onClick="deleteDropRow('<?php echo $tempField->id; ?>','<?php echo $tempField->field_id; ?>')">Remove</a></td>
				</tr>

		   	<?php } }


		function addMultiForm($id,$field_id) { ?>

			<?php 

		   		$tempField = $this->getrowTempFields($id,$field_id);
		   		if(count($tempField)!=0) {
		   		
		   			
		   	?>

		   		<tr id="tr<?php echo $tempField->id; ?>_<?php echo $tempField->field_id; ?>">
					<td class="datath1"><input class="inputs" type="text" name="product_option[<?php echo $tempField->field_id; ?>][row_value][<?php echo $tempField->id; ?>][multi_option_row_title]" id="dropdowntitle" /></td>
					<td class="datath2">
			    		<input class="inputs" type="text" name="product_option[<?php echo $tempField->field_id; ?>][row_value][<?php echo $tempField->id; ?>][multi_option_row_price]" id="price<?php echo $tempField->field_id; ?>__<?php echo $tempField->id; ?>" onChange="PriceOnly(this.id);" />
			        </td>
					<td class="datath3">
						<select class="select_is_required inputs" name="product_option[<?php echo $tempField->field_id; ?>][row_value][<?php echo $tempField->id; ?>][multi_option_row_price_type]" id="dropdownprice_type">
	                		<option value="fixed"><?php echo _e('Fixed','fmepco'); ?></option>
	                		<option value="percent"><?php echo _e('Percent','fmepco'); ?></option>
	                	</select>
					</td>
					<td class="datath4"><input class="inputs" type="text" name="product_option[<?php echo $tempField->field_id; ?>][row_value][<?php echo $tempField->id; ?>][multi_option_row_sort_order]" id="sort_order<?php echo $tempField->field_id; ?>__<?php echo $tempField->id; ?>" onChange="SortOrderonlyNumber(this.id);" /></td>
					<td><a href="javascript:void(0)" onClick="deleteDropRow('<?php echo $tempField->id; ?>','<?php echo $tempField->field_id; ?>')">Remove</a></td>
				</tr>

		   	<?php } }


		 


		


		function addoptionTemData() {
			global $wpdb;
			$wpdb->query($wpdb->prepare( 
	            "
	            INSERT INTO $wpdb->fmepco_temp_table
	            (field_type,field)
	            VALUES (%s,%s)
	            ",
	            'option',
	            'textfield'
	            ) );
			$lastid = $wpdb->insert_id;
			$this->addForm($lastid);
			die();
			return true;

			
			

		}

		function addrowTemData() {
			global $wpdb;
			$wpdb->query($wpdb->prepare( 
	            "
	            INSERT INTO $wpdb->fmepco_temp_table
	            (field_id,field_type,field)
	            VALUES (%s,%s,%s)
	            ",
	            intval($_POST['field_id']),
	            'row',
	            'textfield'
	            ) );
			$lastid = $wpdb->insert_id;
			$this->addForm2($lastid,intval($_POST['field_id']));
			die();
			return true;

		}


		function addmultirowTemData() {
			global $wpdb;
			$wpdb->query($wpdb->prepare( 
	            "
	            INSERT INTO $wpdb->fmepco_temp_table
	            (field_id,field_type,field)
	            VALUES (%s,%s,%s)
	            ",
	            intval($_POST['field_id']),
	            'row',
	            'textfield'
	            ) );
			$lastid = $wpdb->insert_id;
			$this->addMultiForm($lastid,intval($_POST['field_id']));
			die();
			return true;

		}

		


		

		function deloptionTempData() {

			$field_id = intval($_POST['field_id']);
			global $wpdb;
			$wpdb->query( $wpdb->prepare( "DELETE FROM ".$wpdb->fmepco_temp_table . " WHERE id = %d", $field_id ) );
			die();
			return true;

		}

		function delrowTempData() {

			$field_id = intval($_POST['field_id']);
			$id = intval($_POST['id']);
			global $wpdb;
			$wpdb->query( $wpdb->prepare( "DELETE FROM ".$wpdb->fmepco_temp_table . " WHERE id = %d AND field_id = %d", $id, $field_id ) );
			die();
			return true;

		}
		function delrowTempDataFieldOnly($id, $field_id) {

			//$field_id = intval($_POST['field_id']);
			//$id = intval($_POST['id']);
			global $wpdb;
			$wpdb->query( $wpdb->prepare( "DELETE FROM ".$wpdb->fmepco_temp_table . " WHERE id = %d AND field_id = %d", $id, $field_id ) );
			die();
			return true;

		}

		

		


		
	}

	new FME_Product_Custom_Options_Admin();
}