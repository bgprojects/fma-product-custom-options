<?php

class wp_wc_fmeaddon_product_api_custom_opt_Endpoint{
	
	/** Hook WordPress
	*	@return void
	*/
	public function __construct(){
		add_filter('query_vars', array($this, 'add_query_vars'), 0);
		add_action('parse_request', array($this, 'sniff_requests'), 0);
		add_action('init', array($this, 'add_endpoint'), 0);
	}	
	
	/** Add public query vars
	*	@param array $vars List of current public query vars
	*	@return array $vars 
	*/
	public function add_query_vars($vars){
		$vars[] = '__api_wp_fma_custom_options_product_api';
		$vars[] = 'id';
		return $vars;
	}
	
	/** Add API Endpoint
	*	@return void
	*/
	public function add_endpoint(){
		add_rewrite_rule('^api/wp_wc_fma_custom_options_product/?([0-9]+)?/?','fmeaddon-product-api-endpoint.php?__api_wp_fma_custom_options_product_api=1&id=$1','top');//?__api=1&id=$1
	}
	/**	Sniff Requests
	*	This is where we hijack all API requests
	* 	If $_GET['__api'] is set, we kill WP
	*	@return die if API request
	*/
	public function sniff_requests(){
		global $wp;
		if(isset($wp->query_vars['__api_wp_fma_custom_options_product_api'])){
			$this->handle_request();
			exit;
		}
	}
	
	/** Handle Requests
	*	@return void 
	*/
	
	public function wcafp_check_filesize($file,$check){
		/*if($check['max_file_size'] != "" && $check['max_file_size'] != 0) {
			$max_file_size = $check['max_file_size'] * 1048576;
			if($file['size'] > $max_file_size) {
				$return['errors'][] = __( 'File size too big', 'wcafp' );
			}
		}*/
	}
	
	public function wcafp_process_file($file){
		$dir =  wp_upload_dir()['basedir'] . DIRECTORY_SEPARATOR . 'wp_fma_custom_products_upl' . DIRECTORY_SEPARATOR;
		$new_filename = $file['name'];
		$new_location = $dir . DIRECTORY_SEPARATOR . $new_filename;
		return $this->wcafp_make_previews($new_location, $new_filename, $file);
	}
	protected function handle_request(){
		global $wp;
		$data = array();

		$id_post = $wp->query_vars['id'];
		if(!$id_post || $id_post == ""){
			$this->send_response('Missing id argument.');
		}
		
		if($id_post == "image_add"){
			if(isset($_FILES) && count($_FILES) > 0)
			{
				$error = false;
				$files = "";
				$files_added = array();


				$uploaddir =  wp_upload_dir()['basedir'] . DIRECTORY_SEPARATOR . 'wp_fma_custom_products_upl' . DIRECTORY_SEPARATOR;
				$uploaddir_base_url = '/wp_fma_custom_products_upl/';
				if (!file_exists($uploaddir)) {
					mkdir($uploaddir, 0777, true);
				}
				
				foreach($_FILES as $file)
				{
					$check = $file;
					if(move_uploaded_file($file['tmp_name'], $uploaddir .basename($file['name'])))
					{
						$files = $uploaddir_base_url .basename($file['name']);
						$data[] = $files;
					}
					else
					{
						$error = true;
					}
				}
				//$key2 = '_wp_wc_price_upload_file_file_upload';
				//update_post_meta( $id_post, $key2, json_encode($data));
				
				$this->send_response('200 OK', "data", json_encode($data));
			}else{
				$this->send_response('Invalid data');
			}
		}else{
			if($id_post == "update_meta"){
				$post_id = $_POST["post_id"];
				$data_all = array();
				foreach ($_POST as $key => $value){
					if($key != "post_id"){
						$data_offf = explode("___FILE___", $value);
						if($data_offf[0] != "" && $data_offf[1] != ""){
							$arr_data = array("key" => $key, "label" => $data_offf[0], "file" => $data_offf[1], "shadow_file" => $data_offf[2], "file2" => $data_offf[3], "shadow2_file" => $data_offf[4]);
							$data_all[] = $arr_data;
						}
					}
				}
				if(count($data_all) > 0){
					update_option( "wp_wc_fma_product_cust_options_formats", $data_all);
				}
				
				$this->send_response('200 OK', "data", "");
			}else{
				$this->send_response('Invalid data');
			}
		}
		/*$aapi = $wp->query_vars['__api'];
		$id_data = $wp->query_vars['id'];
		if(!$id_data || intval($id_data) === 0){
			$this->send_response('Missing id argument.');
		}
		$id_numb = intval($id_data);
		if(isset($_POST['name']) && $_POST['name'] != "" && 
		isset($_POST['value']) && $_POST['value'] != ""){
			$nameEscaped = esc_html($_POST['name']);
			$valueEscaped = esc_html($_POST['value']);
			$updatedsuccessfully = $this->updateDataInDatabase($id_numb, $nameEscaped, $valueEscaped);
			if($updatedsuccessfully !== false){
				$this->send_response('200 OK', $nameEscaped, $valueEscaped);
			}else{
				$this->send_response('Something went wrong');
			}
		}else{
			$this->send_response('Invalid data');
		}*/
	}
	/*protected function updateDataInDatabase($id, $name, $value){
		global $wpdb;
		$table_name = $wpdb->prefix . 'brandgate_task';
		return $wpdb->update( 
			$table_name, 
			array( 
				'name' => $name,
				'value' => $value
			), 
			array( 'id' => $id ), 
			array( 
				'%s',
				'%s'
			), 
			array( '%d' ) 
		);
	}*/
	
	/** Response Handler
	*	This sends a JSON response to the browser
	*/
	protected function send_response($msg, $name="", $value=""){
		$response['value'] = $value;
		$response['name'] = $name;
		$response['message'] = $msg;
		header('content-type: application/json; charset=utf-8');
	    echo json_encode($response)."\n";
	    exit;
	}
}
new wp_wc_fmeaddon_product_api_custom_opt_Endpoint();

?>