<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( !class_exists( 'FME_Product_Custom_Options_FrontView' ) ) { 

	class FME_Product_Custom_Options_FrontView extends FME_Product_Custom_Options {

		public function __construct() {


			

		}


		public function show_price_logged_or_hide( $price ) {
			global $post;
			$meta_info_previews = get_post_meta($post->ID, 'previews_for_chili', true);
			if($meta_info_previews != ""){
				return "";
			}
			return $price;
		}
		public function custom_woocommerce_placeholder_img_src( $src ) {
			$upload_dir = wp_upload_dir();
			$uploads = untrailingslashit( $upload_dir['baseurl'] );
			//$src = $uploads . '/placeholder.png';
			global $post;
			$meta_info_previews = get_post_meta($post->ID, 'previews_for_chili', true);
			$arr_of_previews = array();
			if($meta_info_previews != ""){
				$objdec = json_decode($meta_info_previews);
				if($objdec != null){
					$arr_of_previews = (array)$objdec;
				}
			}
			$ret_imgs_src = $this->getCustomOptionsOnHere($arr_of_previews);
			if($ret_imgs_src != ""){
				$src = $ret_imgs_src;
                                $src = str_replace("http://", "//", $src);
			}

			return $src;
		}
		
		public function getCustomOptionsOnHere($arr_of_previews){
			$ret = "";
			global $post;
			$ProductOptions = $this->getProductOptions($post->ID);
			if($ProductOptions!='') { 
				foreach ($ProductOptions as $global_option) { 
					$RowOptions = $this->getRowOptions($global_option->id);
					foreach($RowOptions as $option_row) {
						$format_arr = explode(";", $option_row->option_row_sort_order);
						$format_icc = $format_arr[0];
						$id_of_imgs = "";
						if(count($format_arr) > 1){  $id_of_imgs = $format_arr[1]; }
						if($id_of_imgs != ""){
							$previews_arr = $this->getPreviewsOfChiliPreviews($id_of_imgs,$arr_of_previews);
//							if(count($previews_arr) > 0){
//								for($j=0; $j < count($previews_arr); $j++){
//									if($previews_arr[$j] != "" && $ret == ""){
//										$ret = $previews_arr[$j];
//									}
//								}
//								break;
//							}
						}
					}
				}
			}
			return $ret;
		}
		
		
		public function getPreviewsOfChiliPreviews($id, $arr_of_previews){
			$ret = array();
			for($ii=0; $ii < count($arr_of_previews); $ii++){
				$objdec_id = $arr_of_previews[$ii]->id;
				if($objdec_id == $id){
					$objdec_previews = $arr_of_previews[$ii]->previews;
                                        $objdec_previews = str_replace("http://", "//", $objdec_previews);
					$previews = explode(";", $objdec_previews);
					$ret = $previews;
				}
			}
			return $ret;
		}
				
		public function front_scripts() {
            wp_enqueue_style( 'fmepco-front-css', plugins_url( '/css/fmepco_front_style.css', __FILE__ ), false );
            wp_enqueue_script( 'fmepco-accounting-js', plugins_url( '/js/accounting.min.js', __FILE__ ), array( 'jquery' ) );
            wp_enqueue_script( 'wpwc-formatting-js', plugins_url( '/js/format.20110630-1100.min.js', __FILE__ ), array( 'jquery' ) );
        		
        }

        function fme_wc_add_notice($string, $type="error") {
 	
			global $woocommerce;
			if( version_compare( $woocommerce->version, 2.1, ">=" ) ) {
				wc_add_notice( $string, $type );
		} else {
		   $woocommerce->add_error ( $string );
		}
 	
 	
 }

        function ProductCustomOptions() {
        	global $post;
			global $added_already_chili_preview_array;
			$can_show = true;
			if(isset($added_already_chili_preview_array)){
				if(in_array($post->ID, $added_already_chili_preview_array)){
					$can_show = false;
				}
			}else{
				$added_already_chili_preview_array = array();
				$added_already_chili_preview_array[] = $post->ID;
			}
			if($can_show){
				$meta_info_previews = get_post_meta($post->ID, 'previews_for_chili', true);
				$ProductOptions = $this->getProductOptions($post->ID);
				if($meta_info_previews != "" && is_array($ProductOptions) && count($ProductOptions) > 0){
					require  FMEPCO_PLUGIN_DIR . 'front/view/product_custom_options.php';
				}
        	}
        }


        function addlink() { ?>

            <?php }
        

        function getProductOptions($post_id) {

        	global $wpdb;
            $result = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM ".$wpdb->fmepco_poptions_table." WHERE product_id = %d ORDER BY length(option_sort_order), option_sort_order", $post_id));      
            return $result;
        }

        function getRowOptions($option_id) {

        	global $wpdb;
			
            $result = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM ".$wpdb->fmepco_rowoption_table." WHERE option_id = %d ORDER BY length(option_row_sort_order), option_row_sort_order", $option_id));      
            return $result;
        }

        function getRowOptionsByName($option_id, $name) {

        	global $wpdb;
            $result = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM ".$wpdb->fmepco_rowoption_table." WHERE option_row_title = %s AND option_id = %d", $name, $option_id));      
            return $result;
        }

        

        function getProductRequired($post_id,$key) {

        	global $wpdb;
            $result = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM ".$wpdb->fmepco_poptions_table." WHERE product_id = %d AND option_title = %s", $post_id, $key));      
            return $result;
        }

        function addProductToCart( $cart_items,$product_id ) {

			if ( empty( $cart_items['options'] ) ) {
					
					$cart_items['options'] = array();
					
				}

				$array_options = $this->getProductOptions($product_id);
				
				foreach ( $array_options as $options_key => $options ) { 

						$title = strtolower(str_replace(' ', '_', $options->option_title));
						$val_post = $_POST['product_options'][$title];
						$proprice = get_post_meta($product_id, "_price", true);

						if($options->option_price_type == 'percent') {
							$OptionPrice = $proprice*$options->option_price/100;
						} else {
							$OptionPrice = $options->option_price;
						}


						

						if($val_post != '')
						{
							if($options->option_field_type == 'multiple') {
								
								$data[] = array(
									'name'  => $title
									);

								


								foreach ($val_post as $rowvalue) {
									$value = $rowvalue;
									$RowOption = $this->getRowOptionsByName($options->id, $rowvalue); 

									if($RowOption->option_row_price_type == 'percent') {
										$RowOptionPrice = $proprice*$RowOption->option_row_price/100;
									} else {
										$RowOptionPrice = $RowOption->option_row_price;
									}

									$data[] = array(
									'name'  => '',
									'value' => $value,
									'price' => $OptionPrice,
									'option_price' => $RowOptionPrice,

									);
								}
							} else if($options->option_field_type == 'drop_down') {
							
							$value = $val_post;
							$RowOption = $this->getRowOptionsByName($options->id, $val_post); 
							if($RowOption->option_row_price_type == 'percent') {
								$RowOptionPrice = $proprice*$RowOption->option_row_price/100;
							} else {
								$RowOptionPrice = $RowOption->option_row_price;
							}

								$data[] = array(
									'name'  => $title,
									'value' => $value,
									'price' => $OptionPrice,
									'option_price' => $RowOptionPrice,

								);

							} else {

								$value = $val_post;
								

									$data[] = array(
										'name'  => $title,
										'value' => $value,
										'price' => $OptionPrice,
										'option_price' => 0,

									);
							}

							
								
								
								
						}

						$cart_items['options'] =  $data;
					}


				
					//echo "<pre>";
					//print_r($cart_item_data);
					//exit();
					return $cart_items;

		}

		function add_cart_item($cart_items) {
		
			if ( ! empty( $cart_items['options'] ) ) {

				$extra_cost = 0;

				foreach ( $cart_items['options'] as $options ) {
					
					if ( isset($options['price']) && $options['price'] > 0 ) {
						
						$extra_cost += $options['price'];
						
					}

					if ( isset($options['option_price']) && $options['option_price'] > 0 ) {
						
						$extra_cost += $options['option_price'];
						
					}
				}

				$cart_items['data']->adjust_price( $extra_cost );
			}

			return $cart_items;
		}


		function get_cart_item_from_session($cart_items, $values) {
			
			if ( ! empty( $values['options'] ) ) {
				
				$cart_items['options'] = $values['options'];
				
				$cart_items = $this->add_cart_item( $cart_items );
				
			}
			return $cart_items;
		}

		function get_item_data( $other_data, $cart_items ) {

			
			if ( ! empty( $cart_items['options'] ) ) {

				
				foreach ( $cart_items['options'] as $options ) {
									
					$title = ucwords(str_replace('_', ' ', $options['name']));

					if ( isset($options['price'] ) && $options['price'] > 0 ) {
						
						$title .= ' (' . woocommerce_price($this->get_product_addition_options_price($options['price'])) . ')';
					
					}

					if ( isset($options['option_price']) && $options['option_price'] > 0 ) {
						
						$title .= ' (' . woocommerce_price($this->get_product_addition_options_price($options['option_price'])) . ')';
					
					}
					if(isset($options['check']) && $options['check']=='image') {

						$check = 'image';
					} else {
						$check = '';
					}

					if(isset($options['value']) && $options['value']!='') {
						$options_val = $options['value'];
					} else { $options_val = ''; }

				}
			}
			return $other_data;
		}

		

        function ChangeTextAddToCartButton($button, $product) {

        	$CheckProductOptions = $this->getProductOptions($product->id);
        	$is_exclude = get_post_meta ( $product->id, '_exclude_global_options', true );
			if (!in_array($product->product_type, array('variable', 'grouped', 'external'))) {
		        

		        if (count($CheckProductOptions) > 0) {
		            $button = sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" class="button %s product_type_%s">%s</a>',
						esc_url( get_permalink($product->id) ),
						esc_attr( $product->id ),
						esc_attr( $product->get_sku() ),
						$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
						esc_attr( 'variable' ),
						esc_html( __('Select options', 'woocommerce') )
					);
		 
		        }
		    }
 
	 		return $button;
	

        }

        function ValidateCustomOptions($fmedata, $product_id, $qty) { 

        	if($_POST['product_options']!='') {
	        	foreach ($_POST['product_options'] as $key => $value) {
	        		$title = ucwords(str_replace('_', ' ', $key));
	        		$ProductOption = $this->getProductRequired($product_id, $title);
	        		
	        		if($value == '' && $ProductOption->option_is_required == 'yes') {

	        				$fmedata = false;
							$error_message = sprintf ( __ ( '%s is a required field.', 'woocommerce' ), $title );
							$this->fme_wc_add_notice( $error_message );
	        			
	        		}
	        		

	        	}

	        	$CheckProductOptions = $this->getProductOptions($product_id);

	        	
	        	foreach ($CheckProductOptions as $opdata) {
	        		
	        		$title = strtolower(str_replace(' ', '_', $opdata->option_title));
	        		$ProductOption = $this->getProductRequired($product_id, $opdata->option_title);

	        		if(!array_key_exists($title, $_POST['product_options'])) {
		        		if($ProductOption->option_is_required == 'yes') {

		        				$fmedata = false;
								$error_message = sprintf ( __ ( '%s is a required field.', 'woocommerce' ), $opdata->option_title );
								$this->fme_wc_add_notice( $error_message );
		        			
		        		}
	        		}

	        	}


        	}


        	
        
        

        	return $fmedata;
        	

        }


        function order_item_meta($item_id,$values) {


			if ( ! empty( $values['options'] ) ) {
				
				foreach ( $values['options'] as $options ) {


					$name = ucwords(str_replace('_', ' ', $options['name']));

					if ( $options['price'] > 0 ) {
						
						$name .= ' (' . woocommerce_price($this->get_product_addition_options_price( $options['price'] ) ) . ')';
					}

					if ( $options['option_price'] > 0 ) {
						
						$name .= ' (' . woocommerce_price($this->get_product_addition_options_price( $options['option_price'] ) ) . ')';
					}

					if(isset($options['check']) && $options['check']=='image') {

						$check = $options['display'];
					} else {
						$check = $options['value'];
					}

					  wc_add_order_item_meta( $item_id, $name, $check);

					
				}
			}


			
		}


        function get_product_addition_options_price( $price ) {
			
			global $product;

			if ( $price === '' || $price == '0' ) {
				
				return;
				
			}

			if ( is_object( $product ) ) {
				
				$tax_display_mode = get_option( 'woocommerce_tax_display_shop' );
				
				$display_price    = $tax_display_mode == 'incl' ? $product->get_price_including_tax( 1, $price ) : $product->get_price_excluding_tax( 1, $price );
			
			} else {
				
				$display_price = $price;
				
			}

			return $display_price;
		}
	public function get_format_name_from_wp_wc_price_table($prod_id, $format_id){
		global $post;
		
		$ret_name = "";
		$tabledata = $this->getPostTableDataWithAdvanced($prod_id,true);
		for($iij=0; $iij < count($tabledata);$iij++){
			$obj = $tabledata[$iij];
			if(is_object($obj)){
				$arr_vars = $obj->vars;
				for($i=0; $i < count($arr_vars); $i++){
					$ar_vars_obj = $arr_vars[$i];
					$id = $ar_vars_obj->id;
					$name = $ar_vars_obj->name;
					if($id == $format_id){
						$ret_name = $name;
					}
				}
			}
		}
		return $ret_name;
	}
	public function getPostTableDataWithAdvanced($postId,$getTable = false,$onlyTable = false,$selected_paper = null){
		$selected_table = get_post_meta($postId,"_wp-wc-price-table");
		
		if(count($selected_table)){
			global $wpdb;
			$ret = array("postid"=>$postId);
			if($getTable){
			
			
				$table = $wpdb->get_results( 
					"
					SELECT *
					FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["tables"]." 
					WHERE id = ".$selected_table[0]."
					"
				);
				$tables = $table;
                                
				
                                if(count($table) == 0 || !isset($table[0]->id))
                                    return null;//table is not selected or does not exist
								
                foreach ($tables as $table){
					$vars = $wpdb->get_results( 
						"
						SELECT * 
						FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["variables"]." 
							WHERE table_id=".$table->id."
						"
					);
					if(count($vars) > 0){
						
						foreach($vars as $var){
							$opts = $wpdb->get_results( 
								"
								SELECT * 
								FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["options"]." o
									WHERE var_id=".$var->id."
								"
							);

							if(count($opts) > 0){
							   
								foreach($opts as $opt){

									$opts_a = $wpdb->get_results(
											"SELECT * "
											." FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["options_advanced"]." oa"
											." WHERE oa.opt_id=".$opt->id." AND oa.role=1"
											);
									if(count($opts_a)){
										$opt->options_advanced = $opts_a;
									}
								}
								
								$var->options = $opts;
							}
						}
						
						$table->vars = $vars;
					}
				}
				return $tables;
				
				$ret["table"] = $table;
				$ret["table"][0]->quantities = explode(",",$ret["table"][0]->quantities);
				//$ret["table"][0]->shippings = explode(",",$ret["table"][0]->shippings);
				
				if($onlyTable)
					return $ret;
			}
			if($selected_paper == null){
                            $selected_paper = get_post_meta($postId,"_".$this->plugin_name.'_paper');
							if(isset($selected_paper[0])){
								$selected_paper = $selected_paper[0];
							}else{
								$selected_paper = "";
							}
                        }
                        else{
                            //nothing to do
                        }
			if($selected_paper != ""){
				$paper = $wpdb->get_results( 
					"SELECT *
					FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["paper"]." WHERE id=".$selected_paper." LIMIT 1");
				
				if(isset($paper[0])){
					$ret["paper"] = $paper[0];
				}
			}
			$vars = $wpdb->get_results( 
				"
				SELECT *
				FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["variables"]." 
				WHERE table_id = ".$selected_table[0]."
				ORDER BY ord asc
				"
			);
			
			foreach ( $vars as $var ){
				
				if($var->type == "paper" || $var->type == "coverpaper"){
					
					$var->options = $wpdb->get_results( 
						"
						SELECT p.id,CONCAT(p.tykkelse,'".__("gr. ",'wp-wc-price-table')."',p.name) as name, p.xmlname, o.default,p.infopost
						FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["paperprices"]." p
						LEFT JOIN ".$wpdb->prefix . wp_wc_price_table::$data_tables["options"]." o
						ON p.id = o.charge and o.name='paper' and o.var_id=".$var->id."
						WHERE o.weight=1
						ORDER BY o.ord asc
						"
					);
					
					//print_r($var->options);
					
				}
				else{
				
					$var->options = $wpdb->get_results( 
						"
						SELECT *
						FROM ".$wpdb->prefix . wp_wc_price_table::$data_tables["options"]." 
						WHERE var_id = ".$var->id."
						ORDER BY ord asc
						"
					);
				}
			}
			
			$ret["vars"] = $vars;
			
			return $ret;
		}
		else return false;
	}

	}

	new FME_Product_Custom_Options_Front();
}


?>