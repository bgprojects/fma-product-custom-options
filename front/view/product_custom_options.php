<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
require_once FMEPCO_PLUGIN_DIR . 'front/class-fme-product-custom-option-front-view.php';
$custom_options = new FME_Product_Custom_Options_FrontView();

$format_sel = "";
if(isset($_GET['format']) && $_GET['format'] != ""){
	$format_sel = $_GET['format'];
}
if(isset($_POST['format_selected']) && $_POST['format_selected'] != ""){
	$format_sel = $_POST['format_selected'];
}

$ProductOptions = $custom_options->getProductOptions($post->ID);
$meta_info_previews = get_post_meta($post->ID, 'previews_for_chili', true);
if($meta_info_previews != "" && is_array($ProductOptions) && count($ProductOptions) > 0){}else{return;}

$arr_of_previews = array();
			if($meta_info_previews != ""){
				$objdec = json_decode($meta_info_previews);
				if($objdec != null){
					$arr_of_previews = (array)$objdec;
				}
			}

$currency = get_woocommerce_currency();
$string = get_woocommerce_currency_symbol( $currency );
$proprice = get_post_meta($post->ID, "_price", true);
$product_image =get_the_post_thumbnail($post->ID);

if(!function_exists("getPreviewsOfChiliPreviews")){
	function getPreviewsOfChiliPreviews($id, $arr_of_previews){
		$ret = array();
		for($ii=0; $ii < count($arr_of_previews); $ii++){
			$objdec_id = $arr_of_previews[$ii]->id;
			if($objdec_id == $id){
				$objdec_previews = $arr_of_previews[$ii]->previews;
                                $objdec_previews = str_replace("http://", "//", $objdec_previews);
				$previews = explode(";", $objdec_previews);
				$ret = $previews;
			}
		}
		return $ret;
	}
}
if(!function_exists("getFormatOfChiliPreviews")){
	function getFormatOfChiliPreviews($id, $arr_of_previews){
		$ret = array();
		for($ii=0; $ii < count($arr_of_previews); $ii++){
			$objdec_id = $arr_of_previews[$ii]->id;
			if($objdec_id == $id){
				$ret["format_value"] = $arr_of_previews[$ii]->format_value;
				$ret["displayValue"] = $arr_of_previews[$ii]->displayValue;
				$ret["width"] = preg_replace("/[^0-9]/","",$arr_of_previews[$ii]->width);
				$ret["height"] = preg_replace("/[^0-9]/","",$arr_of_previews[$ii]->height);
			}
		}
		return $ret;
	}
}
if (!function_exists("formatIsSquare")) {

    function formatIsSquare($id_of_imgs,$arr_of_previews,$format_sel) {
        
        $formats_arr_two = getFormatOfChiliPreviews($id_of_imgs, $arr_of_previews);
        $previews_arr = getPreviewsOfChiliPreviews($id_of_imgs, $arr_of_previews);
        
        if ($format_sel == null) { // if no format selected yet
            if ($formats_arr_two["width"] == $formats_arr_two["height"]) { // if format is square
                if (count($previews_arr) > 2) { // if is 4 side format
                    return true;
                }
            }
        }
    }

}
?>

<div data-pid="<?php echo $post->ID; ?>" class="custom_options previews_chili_format_selection_on" style="display:none;">

	

	<!-- Product Options Start-->
	<?php if($ProductOptions!='') { ?>
	<?php foreach ($ProductOptions as $global_option) { ?>
	<?php 
		$title = strtolower(str_replace(' ', '_', $global_option->option_title));

		if(isset($_POST['product_options'][$title]) && $_POST['product_options'][$title]!='') {
			$val_post = $_POST['product_options'][$title];
		} else { $val_post = ''; }
	?>

        <div data-id="<?php echo $global_option->id; ?>" id="fme_custom_group_chili_go<?php echo $global_option->id; ?>" class="fmecustomgroup" style="display: none">	
            <!-- <p class="text_over_formats"><?php _e("Klikk på ikonene for å velge format"); ?></p> -->
		<?php /*<label>
			<?php echo stripslashes($global_option->option_title); ?> 
			<?php if($global_option->option_is_required == 'yes') { ?>
				<span class="required">*</span>
			<?php } ?>
			:-
			<?php if($global_option->option_price != '') { ?>
				<?php if($global_option->option_price_type == 'percent') { ?>
					<span class="price">(
						<?php
							echo wc_price($proprice*$global_option->option_price/100, array(
							    'ex_tax_label'       => false,
							    'currency'           => '',
							    'decimal_separator'  => wc_get_price_decimal_separator(),
							    'thousand_separator' => wc_get_price_thousand_separator(),
							    'decimals'           => wc_get_price_decimals(),
							    'price_format'       => get_woocommerce_price_format()
							) );

						
						?>
					)</span>
				<?php } else { ?>
					<span class="price">(
						<?php 


							echo wc_price($global_option->option_price, array(
							    'ex_tax_label'       => false,
							    'currency'           => '',
							    'decimal_separator'  => wc_get_price_decimal_separator(),
							    'thousand_separator' => wc_get_price_thousand_separator(),
							    'decimals'           => wc_get_price_decimals(),
							    'price_format'       => get_woocommerce_price_format()
							) );



						 ?>
					)</span>
				<?php } ?>
			<?php } ?>
		</label>*/?>
		<?php if($global_option->option_field_type == 'field') { ?>
			<input data-price="<?php if($global_option->option_price_type == 'percent') { echo $proprice*$global_option->option_price/100; } else { echo $global_option->option_price; }  ?>" class="fmeop fmeinput" type="text" maxlength="<?php echo $global_option->option_maxchars; ?>" value="<?php if( ! empty($val_post) ){ echo $val_post; } ?>" name="product_options[<?php echo strtolower(str_replace(' ', '_', $global_option->option_title)); ?>]">
		<?php } else if($global_option->option_field_type == 'area') { ?>
			<textarea data-price="<?php if($global_option->option_price_type == 'percent') { echo $proprice*$global_option->option_price/100; } else { echo $global_option->option_price; }  ?>" class="fmeop fmeinput" maxlength="<?php echo $global_option->option_maxchars; ?>" name="product_options[<?php echo strtolower(str_replace(' ', '_', $global_option->option_title)); ?>]"><?php if( ! empty($val_post) ){ echo $val_post; } ?></textarea>
		<?php } else if($global_option->option_field_type == 'drop_down') { ?> 
			
			<?php 
			$RowOptions = $this->getRowOptions($global_option->id);
			$dir_plugin = plugin_dir_url( __FILE__ );
			$imgs_assets = $dir_plugin."img/";
			?>
		<?php 
		$arr_fmrs = get_option("wp_wc_fma_product_cust_options_formats");
		$ij_nr = 0; $count_nrr = 0; 
		if(count($RowOptions) > 6){
			?>
			<div style="display:none;" class="arrow_left arrow_left_go<?php echo $global_option->id; ?> arrow_for_both_in_chili_formats" onclick="arrow_left_on_chili_go(<?php echo $global_option->id; ?>);">
				<svg viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow"></path></svg>
			</div>
			<?php
		}
		foreach($RowOptions as $option_row) {
			$count_nrr++;
                        $selected_format = false;
			$format_arr = explode(";", $option_row->option_row_sort_order);
			$title_showing_now = str_replace("alternative_chili_id", "", $option_row->option_row_title);
			$tit_export_go = explode("-", $title_showing_now);
			$format_icc = $format_arr[0];
			$img_all_href = $imgs_assets.$format_icc.".png";
                        $shadow_img_all_href = "";
			if($arr_fmrs != "" && count($arr_fmrs) > 0){
				for($uzi=0; $uzi < count($arr_fmrs); $uzi++){
					if($arr_fmrs[$uzi]["key"] == $format_icc){
                                            $img_all_href = wp_upload_dir()['baseurl']."/".$arr_fmrs[$uzi]["file"];
                                            if(isset($arr_fmrs[$uzi]["file2"]) && $arr_fmrs[$uzi]["file2"] != "") {
                                                $img_icon = wp_upload_dir()['baseurl']."/".$arr_fmrs[$uzi]["file2"];
                                            } else { 
                                                $img_icon = "";
                                            }
                                            if(isset($arr_fmrs[$uzi]["shadow2_file"]) && $arr_fmrs[$uzi]["shadow2_file"] != "") {
                                                $img_icon_inactive = wp_upload_dir()['baseurl']."/".$arr_fmrs[$uzi]["shadow2_file"];
                                            } else {
                                                $img_icon_inactive = "";
                                            }
                                            if(isset($arr_fmrs[$uzi]["shadow_file"]) && $arr_fmrs[$uzi]["shadow_file"] != "") {
                                                $shadow_img_all_href = wp_upload_dir()['baseurl'].$arr_fmrs[$uzi]["shadow_file"];
                                            }
					}
				}
			}
			$id_of_imgs = "";
			$rotate_how_much = "";
            $folding = "";
            $wp_wcp_price_id = "";
			if(count($format_arr) > 1){  $id_of_imgs = $format_arr[1]; }
			if(count($format_arr) > 2){  $rotate_how_much = $format_arr[2]; }
			if(count($format_arr) > 3){  $folding = $format_arr[3]; }
			if(count($format_arr) > 4){  $wp_wcp_price_id = $format_arr[4]; }
			
			if($id_of_imgs != ""){
				$previews_arr = getPreviewsOfChiliPreviews($id_of_imgs,$arr_of_previews);
				if(count($previews_arr) > 0){
					echo "<div style='display:none;' class='previews_for_that_chili_imgs".$format_icc."'>";
					for($j=0; $j < count($previews_arr); $j++){
						if($previews_arr[$j])
                                                    echo $previews_arr[$j].";";
					}
					echo "</div>";
				}
			}
			
		?>
		
			<div data-id="<?php echo $global_option->id; ?>" class="chili_formats_imgs_on_here_div<?php echo $global_option->id; ?> chili_container_for_format" style="<?php if($count_nrr > 6){ echo "display:none;"; } ?>">
                            <div data-img="<?php echo $format_icc; ?>" class="choose_format_here_chili here_format_chili_imgs<?php echo $format_icc; 
                            if(formatIsSquare($id_of_imgs,$arr_of_previews,$format_sel)) { 
                                echo ' choose_format_here_chili_selected ';
                                $format_sel = $format_icc;
                            };
                            if($ij_nr+1 == count($RowOptions) && ($format_sel == "" || $format_sel == null) ) { // if no format is selected at the end of the loop - add class notSquare
                                echo ' notSquare ';
                            }
                            
                                $ij_nr++;
                            ?>">
				<img  src="<?php echo $img_all_href; ?>" alt="">
				<?php
                                
				if($id_of_imgs != ""){
					$formats_arr_two = getFormatOfChiliPreviews($id_of_imgs,$arr_of_previews);
					$height_format = 100;
					$width_format = 100;
                                        $title_showing_now = str_replace("alternative_chili_id", "", $option_row->option_row_title);
					if(isset($formats_arr_two["width"]) && $formats_arr_two["width"] != ""){
						$height_format = $formats_arr_two["height"];
						$width_format = $formats_arr_two["width"];
					}
					if(count($formats_arr_two) > 0){
                                            if($selected_format){
                                                //selected
                                                echo "<script>var default_height = ".$height_format.";var default_width = ".$width_format.";</script>";
                                                $selected_format = false;
                                            }
                                            
						echo "<div style='display:none;' class='rotate_how_much'>".$rotate_how_much."</div>";
						echo "<div style='display:none;' class='wp_wcp_price_id'>".$wp_wcp_price_id."</div>";
						if($wp_wcp_price_id != ""){
							$wp_wcp_price_id_title = $custom_options->get_format_name_from_wp_wc_price_table($post->ID, $wp_wcp_price_id);
							echo "<div style='display:none;' class='wp_wcp_price_id_title'>".$wp_wcp_price_id_title."</div>";
						}
						echo "<div style='display:none;' class='fma_folding'>".$folding."</div>";
						echo "<div style='display:none;' class='format_value'>".$formats_arr_two["format_value"]."</div>";
						echo "<div style='display:none;' class='displayValue'>".$formats_arr_two["displayValue"]."</div>";
						echo "<div style='display:none;' class='width'>".$formats_arr_two["width"]."</div>";
						echo "<div style='display:none;' class='height'>".$formats_arr_two["height"]."</div>";
//                                                echo "<div class='lightboxFormatSize'>".$formats_arr_two["width"]. " x " .$formats_arr_two["height"]."</div>";
                                                if ($id_of_imgs != "") {
                                                    $previews_arr = getPreviewsOfChiliPreviews($id_of_imgs, $arr_of_previews);
                                                    if (count($previews_arr) > 0) {
                                                        echo "<div style='display:none;' class='previewSources" . $format_icc . "'>";
                                                        for ($j = 0; $j < count($previews_arr); $j++) {
                                                            if ($previews_arr[$j])
                                                                echo $previews_arr[$j] . ";";
                                                        }
                                                        echo "</div>";
                                                    }
                                                }
                                            }
				}
				?>
                                <?php if($shadow_img_all_href != ""){ ?>
                                <input type="hidden" class="hiddenShadowImg" value="<?php echo $shadow_img_all_href; ?>">
                                <?php } ?>
                                <?php if(isset($img_icon)){ ?>
                                <input type="hidden" class="hiddenIcon" value="<?php echo $img_icon; ?>">
                                <?php } ?>
                                <?php if(isset($img_icon_inactive)){ ?>
                                <input type="hidden" class="hiddenIconInactive" value="<?php echo $img_icon_inactive; ?>">
                                <?php } ?>
					<div class="sider_go_fma_product_title"><?php if(count($tit_export_go) > 1){ echo($tit_export_go[1]); }else{ echo($tit_export_go[0]);  } ?></div>
				</div>
			</div>
		<?php } 
		if($count_nrr > 6){
			?>
                            <div class="arrow_right arrow_right_go<?php echo $global_option->id; ?> arrow_for_both_in_chili_formats" onclick="arrow_right_on_chili_go(<?php echo $global_option->id; ?>);">
				<svg viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow" transform="translate(100, 100) rotate(180) "></path></svg>
			</div>
			<?php
		}
		?>
		<?php $ij_nr = 0; foreach($RowOptions as $option_row) { 
			$format_arr = explode(";", $option_row->option_row_sort_order);
			$format_icc = $format_arr[0];
			$id_of_imgs = "";
			if(count($format_arr) > 1){  $id_of_imgs = $format_arr[1]; }
			$title_showing_now = str_replace("alternative_chili_id", "", $option_row->option_row_title);
				if($id_of_imgs != ""){
					$formats_arr_two = getFormatOfChiliPreviews($id_of_imgs,$arr_of_previews);
					if(count($formats_arr_two) > 0){
						if(isset($formats_arr_two["width"]) && $formats_arr_two["width"] != ""){
							//$title_showing_now = $formats_arr_two["width"]." X ".$formats_arr_two["height"];
						}
					}
				}
			if(($ij_nr == 1 && $format_sel == "") || ($format_sel != "" && $format_sel == $format_icc)){
				echo "<div style='display:none;' class='format_selected_in_chili_now'>".$format_icc."</div>";
			}
                        //style="<?php if(($ij_nr == 0 && $format_sel == "") || ($format_sel != "" && $format_sel == $format_icc)){ echo "display:block;"; $ij_nr++; } >"
		?>
		<div class="choose_format_here_chili_title here_format_chili_titles<?php echo $format_icc;  if(($ij_nr == 0 && $format_sel == "") || ($format_sel != "" && $format_sel == $format_icc)){ echo " format_title_selected"; $ij_nr++; } ?>">
                    <a data-prod="<?php echo $post->ID; ?>" href="#" class="formatlink">
                        <p class="formatTitle"><span style="color:black;"><?php _e("Nå vises:"); ?></span> <?php echo $title_showing_now; ?></p>
                    </a>
                    <?php 
                    if(class_exists("wp_wc_price_table_Public")){ ?>
                        <p class="productprice">
                            <span><?php _e("Priseks.:");?></span>
                            <span class="smallqty">30 stk kr</span>
                            <span class="smalloldprice strike">12.90</span>
                            <span class="smallprice">9.60/stk</span>
                            <span><?php _e("inkl konv.");?></span>
                            
                            <?php
                                //connect price table if used
                                echo "<input type='hidden' name='paper' value='".$option_row->option_row_price."'>";
                                $priceTable = new wp_wc_price_table_Public();
		
                                //if($tabledata!==false){
									// echo $priceTable->woocommerce_get_price_html("",new WC_Product($post->ID),$option_row->option_row_price);
									echo $priceTable->woocommerce_get_price_html("",new WC_Product($post->ID),$option_row->option_row_price);
                                //}
                            }else{
                                ?> 
                        </p <!--end of product price-->
                    <span class="price" style="display:none;">
                            <?php 
                            
                            echo wc_price($option_row->option_row_price, array(
                                'ex_tax_label'       => false,
                                'currency'           => '',
                                'decimal_separator'  => wc_get_price_decimal_separator(),
                                'thousand_separator' => wc_get_price_thousand_separator(),
                                'decimals'           => wc_get_price_decimals(),
                                'price_format'       => get_woocommerce_price_format()
                            ) );


                             ?>
                    </span>
		<?php } ?>
		</div>
		<?php } 
		?>
			<select style="display:none;" type="select" class="fma fmeop fmeinput formats_in_chili_choose_format" name="product_options[<?php echo strtolower(str_replace(' ', '_', $global_option->option_title)); ?>]">
				<?php foreach($RowOptions as $option_row) { 
			$format_arr = explode(";", $option_row->option_row_sort_order);
			$format_icc = $format_arr[0];
			$id_of_imgs = "";
			if(count($format_arr) > 1){  $id_of_imgs = $format_arr[1]; }
				?>
				
				
				
					<option data-form-size="<?php echo $format_icc; ?>" <?php selected($val_post,$option_row->option_row_title); ?> data-price="<?php if($option_row->option_row_price_type == 'percent') { echo $proprice*$option_row->option_row_price/100; } else { echo $option_row->option_row_price; }  ?>" value="<?php echo $option_row->option_row_title; ?>">
					<?php echo $option_row->option_row_title; ?>   
					<?php if($option_row->option_row_price != '') { ?>
						<?php if($option_row->option_row_price_type == 'percent') { ?>
							-  <span class="price">(
								<?php 


							echo wc_price($proprice*$option_row->option_row_price/100, array(
							    'ex_tax_label'       => false,
							    'currency'           => '',
							    'decimal_separator'  => wc_get_price_decimal_separator(),
							    'thousand_separator' => wc_get_price_thousand_separator(),
							    'decimals'           => wc_get_price_decimals(),
							    'price_format'       => get_woocommerce_price_format()
							) );


							 ?>
							)</span>
						<?php } else { ?>
							-  <span class="price">(
								<?php 



								echo wc_price($option_row->option_row_price, array(
								    'ex_tax_label'       => false,
								    'currency'           => '',
								    'decimal_separator'  => wc_get_price_decimal_separator(),
								    'thousand_separator' => wc_get_price_thousand_separator(),
								    'decimals'           => wc_get_price_decimals(),
								    'price_format'       => get_woocommerce_price_format()
								) );


								 ?>
							)</span>
						<?php } ?>
					<?php } ?>
					</option>
				<?php } ?>
			</select>
			
		<?php } else if($global_option->option_field_type == 'multiple') { ?> 
			<select type="mselect" multiple = "multiple" class="fmm fmeop fmeinput multi" name="product_options[<?php echo strtolower(str_replace(' ', '_', $global_option->option_title)); ?>][]">
				<?php $RowOptions = $this->getRowOptions($global_option->id) ?>
				<?php foreach($RowOptions as $option_row) { ?>
					<?php 
						$title = strtolower(str_replace(' ', '_', $global_option->option_title));

						if(isset($_POST['product_options'][$title]) && $_POST['product_options'][$title]!='') {
							$val_post2 = $_POST['product_options'][$title];
						} else { $val_post2 = ''; } 

					?>
					<option <?php if($val_post2!='') { foreach ($val_post2 as $valp) { selected($valp,$option_row->option_row_title); } } ?> data-price="<?php if($option_row->option_row_price_type == 'percent') { echo $proprice*$option_row->option_row_price/100; } else { echo $option_row->option_row_price; }  ?>" value="<?php echo $option_row->option_row_title; ?>">
					<?php echo $option_row->option_row_title; ?>   
					<?php if($option_row->option_row_price != '') { ?>
						<?php if($option_row->option_row_price_type == 'percent') { ?>
							-  <span class="price">(
								<?php 


								echo wc_price($proprice*$option_row->option_row_price/100, array(
								    'ex_tax_label'       => false,
								    'currency'           => '',
								    'decimal_separator'  => wc_get_price_decimal_separator(),
								    'thousand_separator' => wc_get_price_thousand_separator(),
								    'decimals'           => wc_get_price_decimals(),
								    'price_format'       => get_woocommerce_price_format()
								) );

								 ?>
							)</span>
						<?php } else { ?>
							-  <span class="price">(
								<?php 

								echo wc_price($option_row->option_row_price, array(
								    'ex_tax_label'       => false,
								    'currency'           => '',
								    'decimal_separator'  => wc_get_price_decimal_separator(),
								    'thousand_separator' => wc_get_price_thousand_separator(),
								    'decimals'           => wc_get_price_decimals(),
								    'price_format'       => get_woocommerce_price_format()
								) );

								 ?>
							)</span>
						<?php } ?>
					<?php } ?>
					</option>
				<?php } ?>
			</select>
		<?php } ?>
                        
	</div>
	<?php } ?>
	<?php } ?>

	<!-- Product Options End-->

</div>        

<?php 
				
			$product = get_product( $post->ID );
				
			if ( is_object( $product ) ) {
				
				$tax_display_mode = get_option( 'woocommerce_tax_display_shop' );
				
				$d_price    = $tax_display_mode == 'incl' ? $product->get_price_including_tax() : $product->get_price_excluding_tax();
			
			} else {
				
				$d_price    = '';
				
			}


?>
<!-- Hide format options on category page -->
  <?php if(!is_shop() || is_product()) { ?> 
<div class="price_total">
    <div class="textAbovePrice"><?php _e("Scroll antall for å se priseksempler på viste kort"); ?></div>
	<p class="text_above_buttons"><?php _e( 'Konvolutter er inkludert i prisen:' ); ?></p>
	<div id="product_options_total" product-type="<?php echo $product->product_type;  ?>" product-price="<?php echo $d_price; ?>">
            <input type="text" class="priceInput" id="totalPriceInput" value="" readonly>
            <p class="price_per_one_chili" id="unitPriceInput"></p>
        </div>
</div>
  <?php } ?>

<script type="text/javascript">

<?php
if (defined('DOING_AJAX') && DOING_AJAX) {
	?>
jQuery( document ).ready(function() {
	var pid_added = [];
	jQuery(".previews_chili_format_selection_on").each(function(){
		var thisitem = jQuery(this).attr("data-pid");
		if(pid_added.indexOf(thisitem) == -1){
			pid_added.push(thisitem);
			jQuery(this).show();
		}
	});
	 if(jQuery(".product-page").length){
		var jq_parent = jQuery(".product-info").find(".format_selected_in_chili_now").parent().parent();
		var sel_format_now = jQuery(".product-info").find(".format_selected_in_chili_now").text();
		if(sel_format_now != ""){
			nowChangeFormatSelected(jq_parent, sel_format_now);
                        jq_parent.find(".here_format_chili_titles"+sel_format_now).addClass("format_title_selected");
		}
	 }
//  jQuery(".choose_format_here_chili").click(function(){
//	  var jq_parent = jQuery(this).parent().parent().parent().parent();
//	  var attr = jQuery(this).attr("data-img");
////		nowChangeFormatSelected(jq_parent, attr);
//		ProductCustomOptions(0);
//  });
});
<?php
}
?>

	jQuery( document ).ready( function($) {
            
            jQuery(".product-page .choose_format_here_chili").click(function(){
			ProductCustomOptions();
		});
	
            jQuery(this).on( 'change', '.product-page select.qty', function() {
                    ProductCustomOptions(1);
            });
            if(jQuery(".product-page").length) {
                ProductCustomOptions(2);
            };
	});
        
	function ProductCustomOptions() {
            if(jQuery(".product-lightbox").length) {
                ProductCustomOptionsLightbox();
                return;
            }
		var option_total = 0;
		var product_price = jQuery('#product_options_total').attr( 'product-price' );
		var product_total_price = 0;
		var final_total = 0;
                var qty = jQuery('.qty').val();
                var priceSelector = "";
                
                if(jQuery(".product-lightbox").length) {
                    priceSelector = "div.product-lightbox .custom_options .fmeop";
                } else {
                    priceSelector = "div.product-page div.product-gallery .custom_options .fmeop";
                }
		
		jQuery(priceSelector).each( function() {
			
			var option_price = 0;
			if(jQuery(this).attr('type') == 'select') {

				option_price = jQuery("option:selected", this).attr('data-price');

			} else if(jQuery(this).attr('type') == 'mselect') {
					
					var sum = option_price;
				    jQuery( "option:selected", this ).each(function() {
				      str = parseFloat(jQuery( this ).attr('data-price'));
				      sum = str+sum;
				    });
				    option_price = sum;

			} else {
			
				option_price = jQuery(this).attr('data-price');
			}
			var value_entered =  jQuery(this).val();
			
			if(value_entered != '' || option_price == 0)
			{
				option_total = parseFloat( option_total ) + parseFloat( option_price );
			}
			
		});
		
		if ( option_total > 0 && qty > 0 && jQuery(".product-page").length && jQuery("div.product-gallery .formats_in_chili_choose_format").length) {
			
			option_total = parseFloat( option_total * qty );

			var price_form = "<?php echo get_option( 'woocommerce_currency_pos' ); ?>";
			var op_price = '';
			
			if(price_form == 'left') {
				op_price = accounting.formatMoney(option_total, { symbol: "<?php echo $string; ?>",  format: "%s%v" }, "<?php echo wc_get_price_decimals(); ?>", "<?php echo wc_get_price_thousand_separator(); ?>", "<?php echo wc_get_price_decimal_separator(); ?>"); // €4.999,99
			} else if(price_form == 'left_space') {
				op_price = accounting.formatMoney(option_total, { symbol: "<?php echo $string; ?>",  format: "%s %v" }, "<?php echo wc_get_price_decimals(); ?>", "<?php echo wc_get_price_thousand_separator(); ?>", "<?php echo wc_get_price_decimal_separator(); ?>"); // €4.999,99
			} else if(price_form == 'right') {
				op_price = accounting.formatMoney(option_total, { symbol: "<?php echo $string; ?>",  format: "%v%s" }, "<?php echo wc_get_price_decimals(); ?>", "<?php echo wc_get_price_thousand_separator(); ?>", "<?php echo wc_get_price_decimal_separator(); ?>"); // €4.999,99
			} else if(price_form == 'right_space') {
				op_price = accounting.formatMoney(option_total, { symbol: "<?php echo $string; ?>",  format: "%v %s" }, "<?php echo wc_get_price_decimals(); ?>", "<?php echo wc_get_price_thousand_separator(); ?>", "<?php echo wc_get_price_decimal_separator(); ?>"); // €4.999,99
			}
			

			if ( product_price ) {

				product_total_price = parseFloat( product_price * qty );
				

			}
			
			final_total = option_total + product_total_price;

			var fi_price = '';
			
			if(price_form == 'left') {
				fi_price = accounting.formatMoney(final_total, { symbol: "<?php echo $string; ?>",  format: "%s%v" }, "<?php echo wc_get_price_decimals(); ?>", "<?php echo wc_get_price_thousand_separator(); ?>", "<?php echo wc_get_price_decimal_separator(); ?>"); // €4.999,99
			} else if(price_form == 'left_space') {
				fi_price = accounting.formatMoney(final_total, { symbol: "<?php echo $string; ?>",  format: "%s %v" }, "<?php echo wc_get_price_decimals(); ?>", "<?php echo wc_get_price_thousand_separator(); ?>", "<?php echo wc_get_price_decimal_separator(); ?>"); // €4.999,99
			} else if(price_form == 'right') {
				fi_price = accounting.formatMoney(final_total, { symbol: "<?php echo $string; ?>",  format: "%v%s" }, "<?php echo wc_get_price_decimals(); ?>", "<?php echo wc_get_price_thousand_separator(); ?>", "<?php echo wc_get_price_decimal_separator(); ?>"); // €4.999,99
			} else if(price_form == 'right_space') {
				fi_price = accounting.formatMoney(final_total, { symbol: "<?php echo $string; ?>",  format: "%v %s" }, "<?php echo wc_get_price_decimals(); ?>", "<?php echo wc_get_price_thousand_separator(); ?>", "<?php echo wc_get_price_decimal_separator(); ?>"); // €4.999,99
			}

			html = '';
			
			
				//html = html + '<div class="tprice"><div class="leftprice"><?php echo _e("Options Total:","fmepco") ?></div><div class="rightprice optionprice">'+op_price+'</div></div>';
			
			
			if ( final_total ) {
				
				
//					html = html + '<div class="tprice"><div class="leftprice"><?php echo _e("Final Total:","fmepco") ?></div><div class="rightprice finalprice">'+fi_price+'</div></div>';
                                        html = html + '<div class="tprice"><div class="rightprice finalprice">'+fi_price+'</div></div>';
					html = html +  '<span class="price_per_one_chili">kr ' + product_price + '  - 1 <?php echo _e("unit","fmepco") ?>. <span>';
				

			}

			html = html + '</dl>';

			jQuery('#product_options_total').html( html );
				updatePriceWpWcPriceTable();
		} else {
			
			//jQuery('#product_options_total').html( '' );
		}
	}



</script>

<script>
    var URL = "<?php echo FMEPCO_URL; ?>";
</script>

